<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Creates episode entity
 */
class Version20170928190925 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            "CREATE TABLE IF NOT EXISTS `episode` (
                  `id` CHAR(36) NOT NULL,
                  `patient_id` CHAR(36) NOT NULL,
                  `status` TINYINT(4) NOT NULL DEFAULT 0,
                  `subject` VARCHAR(255) NOT NULL,
                  `anamnesis` TEXT NULL DEFAULT NULL,
                  `primary_diagnosis` TEXT NULL DEFAULT NULL,
                  `secondary_diagnosis` TEXT NULL DEFAULT NULL,
                  `additional_information` TEXT NULL DEFAULT NULL,
                  `is_therapy_needed` TINYINT(4) NULL DEFAULT NULL,
                  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                  `updated_at` TIMESTAMP NULL DEFAULT NULL,
                  `deleted_at` TIMESTAMP NULL DEFAULT NULL,
                  PRIMARY KEY (`id`),
                  INDEX `patient_id_idx` (`patient_id` ASC),
                  INDEX `status` (`status` ASC),
                  CONSTRAINT `patient_id`
                    FOREIGN KEY (`patient_id`)
                    REFERENCES `patient` (`id`)
                    ON DELETE NO ACTION
                    ON UPDATE NO ACTION)
                ENGINE = InnoDB
                DEFAULT CHARACTER SET = utf8"
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
