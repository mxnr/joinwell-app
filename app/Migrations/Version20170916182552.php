<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Migrations\Version;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170916182552 extends AbstractMigration
{
    /**
     * Maximum packet size that allowed per query
     *
     * @var int
     */
    protected $maxPacketSize = 4 * 1024 * 1024;

    /**
     * Data directory path
     *
     * @var string
     */
    protected $myPath = '';

    /**
     * List of tables, that should be processed
     *
     * @var array
     */
    protected $tablesList = [
        'address_info',
        'person',
        'contact_info',
        'user',
        'user_file',

        'therapist',
        'therapist_availability',
        'therapist_note',

        'patient',
        'patient_location',
    ];

    protected function getCurrentFileName(): string
    {
        return __FILE__;
    }

    /**
     * Version20170916182552 constructor.
     *
     * @param Version $version
     *
     * @throws \RuntimeException
     */
    public function __construct(Version $version)
    {
        parent::__construct($version);

        $currentPathInfo = (object)pathinfo($this->getCurrentFileName());
        $this->myPath    = $currentPathInfo->dirname.DIRECTORY_SEPARATOR.$currentPathInfo->filename;

        if (is_dir($this->myPath) === false) {
            throw new \RuntimeException(
                'something terrible has happened, i can\'t find directory with the data <![CDATA['.$this->myPath.']]>'
            );
        }

        //there is we check, that all data that will be processed is available
        foreach ($this->tablesList as $table) {
            $tableFile = $this->myPath.DIRECTORY_SEPARATOR.$table.'.sql';

            if (is_file($tableFile) === false) {
                throw new \RuntimeException(
                    'something terrible has happened, i can\'t find data file for <![CDATA['.$table.']]>'
                );
            }
        }
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        //increase packet maximum size for mysql
        //processing tables
        foreach ($this->tablesList as $table) {
            $tableFile = $this->myPath.DIRECTORY_SEPARATOR.$table.'.sql';

            $sql = file_get_contents($tableFile);

            if (strlen($sql) < $this->maxPacketSize) {
                $this->addSql($sql);
            } else {
                $dataWrapperStart = 'LOCK TABLES `'.$table.'` WRITE;';
                $dataWrapperEnd   = 'UNLOCK TABLES;';

                $structureBeginPos = 0;
                $structureEndPos   = strpos($sql, $dataWrapperStart);
                //determining size of structure in the packet
                $dataBeginPos = strpos($sql, $dataWrapperStart) + strlen($dataWrapperStart);
                $dataEndPos   = strpos($sql, $dataWrapperEnd);

                $structureQuery = substr($sql, $structureBeginPos, $structureEndPos - $structureBeginPos);
                $dataQuery      = substr($sql, $dataBeginPos, $dataEndPos - $dataBeginPos);

                unset($sql);
                $this->addSql($structureQuery);

                $this->addSql($dataWrapperStart);
                foreach (explode(PHP_EOL, $dataQuery) as $insertStatement) {
                    if (empty($insertStatement) === false) {
                        $this->addSql($insertStatement);
                    }
                }
                $this->addSql($dataWrapperEnd);

            }
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        foreach (array_reverse($this->tablesList) as $table) {
            $this->addSql('drop table if exists `'.$table.'`;');
        }

    }
}
