<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Modifying patient table, according to normal practices
 */
class Version20170923042230 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('ALTER TABLE `patient` DROP FOREIGN KEY `fk_patient_user_id`;');
        $this->addSql('ALTER TABLE `patient` ADD COLUMN `user_id` CHAR(36) NOT NULL AFTER `id`;');
        $this->addSql('update patient set user_id = id;');
        $this->addSql(
            'ALTER TABLE `patient` 
                    ADD CONSTRAINT `fk_patient_user_id`
                      FOREIGN KEY (`user_id`)
                      REFERENCES `user` (`id`)
                      ON DELETE NO ACTION
                      ON UPDATE NO ACTION'
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
