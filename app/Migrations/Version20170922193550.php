<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Preparing user for consistent state
 */
class Version20170922193550 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        //alter user table
        $this->addSql(
            'ALTER TABLE `user` 
                    ADD COLUMN `username` VARCHAR(100) NULL DEFAULT NULL AFTER `id`,
                    ADD COLUMN `username_canonical` varchar(180) NOT NULL AFTER `username`,
                    ADD COLUMN `email` VARCHAR(255) NOT NULL AFTER `username_canonical`,
                    ADD COLUMN `email_canonical` varchar(180) NOT NULL AFTER `email`,
                    ADD COLUMN `password` VARCHAR(72) NOT NULL AFTER `email_canonical`,
                    ADD COLUMN `confirmation_token` varchar(180) DEFAULT NULL AFTER `password`,
                    ADD COLUMN `enabled` tinyint(4) NOT NULL AFTER `confirmation_token`,
                    ADD COLUMN `salt` varchar(255) DEFAULT NULL AFTER `enabled`,
                    ADD COLUMN `last_login` timestamp NULL DEFAULT NULL after `salt`,
                    ADD COLUMN `password_requested_at` datetime DEFAULT NULL after `last_login`,
                    ADD COLUMN `roles` longtext NOT NULL COMMENT \'(DC2Type:array)\' after `password_requested_at`'
        );

        //migrate required data
        $this->addSql(
            'update user u join person p on u.person_id = p.id set u.email = p.email, u.password = p.password;'
        );

        //filling up username field
        $this->addSql(
            "update user set username = email, username_canonical = email, email_canonical=email;"
        );

        //creating index
        $this->addSql(
            'ALTER TABLE `user` ADD UNIQUE INDEX `email_UNIQUE` (`email` ASC)'
        );

        $this->addSql(
            'CREATE UNIQUE INDEX UNIQ_9D83D648A1D16FBF ON user (username);'
        );

        $this->addSql(
            'CREATE UNIQUE INDEX UNIQ_8D93D649A0D96FBF ON user (email_canonical);'
        );

        $this->addSql(
            'CREATE UNIQUE INDEX UNIQ_8D93D64992FC23A8 ON user (username_canonical);'
        );

        //clearing patient from unwanted fields
        $this->addSql(
            'ALTER TABLE `person` 
                    DROP COLUMN `forgot_password_token_expiration`,
                    DROP COLUMN `forgot_password_token`,
                    DROP COLUMN `password`,
                    DROP COLUMN `email`,
                    DROP INDEX `email_UNIQUE`'
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
