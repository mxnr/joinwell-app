<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Adding zoom info data
 */
class Version20170930230131 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            'ALTER TABLE .`user` 
                COLLATE = utf8_general_ci ,
                ADD COLUMN `zoom_id` VARCHAR(100) NULL DEFAULT NULL AFTER `person_id`,
                ADD INDEX `zoomId` (`zoom_id` ASC)'
        );


        $this->addSql(
            'CREATE TABLE IF NOT EXISTS `zoom_meeting` (
                  `id` VARCHAR(100) NOT NULL,
                  `host_id` VARCHAR(100) NOT NULL,
                  `client_id` VARCHAR(100) NOT NULL,
                  `status` TINYINT(4) NOT NULL DEFAULT 0,
                  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                  `scheduled_at` TIMESTAMP NOT NULL,
                  `updated_at` TIMESTAMP NULL DEFAULT NULL,
                  `deleted_at` TIMESTAMP NULL DEFAULT NULL,
                  PRIMARY KEY (`id`),
                  INDEX `host_idx` (`host_id` ASC),
                  INDEX `client_idx` (`client_id` ASC),
                  INDEX `status_host` (`status` ASC, `host_id` ASC),
                  INDEX `status_client` (`status` ASC, `client_id` ASC),
                  CONSTRAINT `host`
                    FOREIGN KEY (`host_id`)
                    REFERENCES `user` (`zoom_id`)
                    ON DELETE NO ACTION
                    ON UPDATE NO ACTION,
                  CONSTRAINT `client`
                    FOREIGN KEY (`client_id`)
                    REFERENCES `user` (`zoom_id`)
                    ON DELETE NO ACTION
                    ON UPDATE NO ACTION)
                ENGINE = InnoDB
                DEFAULT CHARACTER SET = utf8'
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
