<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Modifying table therapist, divided pk, and user o-to-o relation
 */
class Version20170921213144 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            '
                ALTER TABLE `therapist`
                DROP FOREIGN KEY `fk_therapist_referred_by`,
                DROP FOREIGN KEY `fk_therapist_user_id`;'
        );

        $this->addSql(
            "
                            ALTER TABLE `therapist` 
                            ADD COLUMN `user_id` CHAR(36) NULL DEFAULT NULL AFTER `id`,
                            ADD UNIQUE INDEX `user_id_UNIQUE` (`user_id` ASC);"
        );


        $this->addSql("update therapist set user_id = id;");

        $this->addSql(
            "
                    ALTER TABLE `therapist`
                    ADD CONSTRAINT `fk_therapist_referred_by`
                      FOREIGN KEY(`referred_by`)
                      REFERENCES `user` (`id`)
                      ON DELETE NO ACTION
                      ON UPDATE NO ACTION,
                    ADD CONSTRAINT `fk_therapist_user_id`
                      FOREIGN KEY(`user_id`)
                      REFERENCES `user` (`id`)
                      ON DELETE NO ACTION
                      ON UPDATE NO ACTION;"
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
    }
}
