<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Change relations of zoom meeting, because the table is empty, just recreate it with the proper params
 */
class Version20171007012610 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('drop table if EXISTS `zoom_meeting`');

        $this->addSql(
            'CREATE TABLE `zoom_meeting` (
                          `id` VARCHAR(100) NOT NULL,
                          `visit_id` CHAR(36) NOT NULL,
                          `zoom_id` VARCHAR(100) NOT NULL,
                          `host_id` CHAR(36) NOT NULL,
                          `host_url` VARCHAR(1000) NOT NULL,
                          `client_id` CHAR(36) NOT NULL,
                          `client_url` VARCHAR(1000) NOT NULL,
                          `status` TINYINT(4) NOT NULL DEFAULT \'0\',
                          `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                          `scheduled_at` TIMESTAMP NOT NULL DEFAULT \'0000-00-00 00:00:00\',
                          `updated_at` TIMESTAMP NULL DEFAULT NULL,
                          `deleted_at` TIMESTAMP NULL DEFAULT NULL,
                          PRIMARY KEY (`id`),
                          INDEX `status_host` (`status` ASC, `host_id` ASC),
                          INDEX `status_client` (`status` ASC, `client_id` ASC),
                          INDEX `zoom_id` (`zoom_id` ASC, `host_id` ASC, `client_id` ASC),
                          INDEX `host_idx` (`host_id` ASC),
                          INDEX `visit_ids` (`visit_id` ASC),
                          INDEX `client_idx` (`client_id` ASC),
                          CONSTRAINT `host`
                            FOREIGN KEY (`host_id`)
                            REFERENCES `user` (`id`)
                            ON DELETE NO ACTION
                            ON UPDATE NO ACTION,
                          CONSTRAINT `client`
                            FOREIGN KEY (`client_id`)
                            REFERENCES `user` (`id`)
                            ON DELETE NO ACTION
                            ON UPDATE NO ACTION,
                          CONSTRAINT `visit`
                            FOREIGN KEY (`visit_id`)
                            REFERENCES `visit` (`id`)
                            ON DELETE NO ACTION
                            ON UPDATE NO ACTION)
                            
                        ENGINE = InnoDB
                        DEFAULT CHARACTER SET = utf8'
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
