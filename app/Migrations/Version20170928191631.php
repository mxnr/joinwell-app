<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Creates visit entity
 */
class Version20170928191631 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            "CREATE TABLE IF NOT EXISTS `visit` (
                  `id` CHAR(36) NOT NULL,
                  `episode_id` CHAR(36) NOT NULL,
                  `therapist_id` CHAR(36) NOT NULL,
                  `status` TINYINT(4) NOT NULL DEFAULT 0,
                  `type` TINYINT(4) NOT NULL DEFAULT 0,
                  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                  `visit_at` TIMESTAMP NOT NULL,
                  `updated_at` TIMESTAMP NULL DEFAULT NULL,
                  `deleted_at` TIMESTAMP NULL DEFAULT NULL,
                  PRIMARY KEY (`id`),
                  INDEX `episode_idx` (`episode_id` ASC),
                  INDEX `therapist_id_idx` (`therapist_id` ASC),
                  INDEX `date` (`visit_at` ASC, `status` ASC),
                  INDEX `status` (`status` ASC, `visit_at` ASC),
                  CONSTRAINT `episode`
                    FOREIGN KEY (`episode_id`)
                    REFERENCES `episode` (`id`)
                    ON DELETE NO ACTION
                    ON UPDATE NO ACTION,
                  CONSTRAINT `therapist_id`
                    FOREIGN KEY (`therapist_id`)
                    REFERENCES `therapist` (`id`)
                    ON DELETE NO ACTION
                    ON UPDATE NO ACTION)
                ENGINE = InnoDB
                DEFAULT CHARACTER SET = utf8"
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
