joinwell
========

A Symfony project created on September 15, 2017, 10:10 pm.

installation
========

Configure mysql for dev station:

```sql
CREATE DATABASE joinwell CHARACTER SET utf8 COLLATE utf8_bin;
CREATE USER 'joinwell'@'%' IDENTIFIED BY 'change_me';
GRANT ALL PRIVILEGES ON joinwell.* TO 'joinwell'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;
```

import actual data:

```bash
php bin/console doctrine:migration:migrate
```

quick erase database:
```sql
SET FOREIGN_KEY_CHECKS = 0;
DROP TABLE address_info, contact_info, migration_versions, patient, therapist, therapist_availability, therapist_note, user, user_file, patient_location, person, visit, episode, zoom_meeting;
SET FOREIGN_KEY_CHECKS = 1;
```
