<?php

namespace JoinWellBundle\Repository;


use JoinWellBundle\Entity\Episode;
use JoinWellBundle\Entity\Patient;

/**
 * Class EpisodeRepository
 * @package JoinWellBundle\Repository
 */
class EpisodeRepository extends AbstractRepository
{
    /**
     * @param Patient $patient
     *
     * @return array
     */
    public function getActiveEpisodesForPatient(Patient $patient): array
    {
        $queryBuilder = $this->createQueryBuilder('e');

        $queryBuilder
            ->andWhere($queryBuilder->expr()->eq('e.patient', ':patient'))
            ->andWhere($queryBuilder->expr()->notIn('e.status', ':status'))
            ->setParameter('status', [Episode::STATUS_SOLVED])
            ->setParameter('patient', $patient->getId());

        return $queryBuilder->getQuery()->getResult();
    }
}
