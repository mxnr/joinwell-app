<?php

namespace JoinWellBundle\Repository;

use JoinWellBundle\Entity\Patient;
use JoinWellBundle\Entity\User;

/**
 * Class PetientRepository
 * @package JoinWellBundle\Repository
 */
class PatientRepository extends AbstractRepository
{
    /**
     * @param User $user
     *
     * @return Patient
     */
    public function getByUser(User $user): Patient
    {
        if ($user->getType() !== User::TYPE_PATIENT) {
            throw new \RuntimeException(sprintf('unsupported user type: <![CDATA[%s]]>', $user->getType()));
        }

        return $this->findOneBy(['user' => $user->getId()]);
    }
}
