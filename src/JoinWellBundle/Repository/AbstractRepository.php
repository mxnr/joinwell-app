<?php

namespace JoinWellBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * Class AbstractRepository
 *
 * @package JoinWellBundle\Repository
 */
class AbstractRepository extends EntityRepository
{
    /**
     * Per page search limit
     *
     * @var int
     */
    protected $limit = 20;

    /**
     * Per result page offset
     *
     * @var int
     */
    protected $offset = 1;

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     *
     * @return AbstractRepository
     */
    public function setLimit(int $limit)
    {
        if ($limit < 1) {
            throw new \InvaligdArgumentException('$limit should be positive value');
        }

        $this->limit = $limit;

        return $this;
    }

    /**
     * Returns offset
     *
     * @return int
     */
    public function getOffset(): int
    {
        return $this->offset;
    }

    /**
     * @param int $offset
     *
     * @return AbstractRepository
     */
    public function setOffset(int $offset)
    {
        if ($offset < 1) {
            throw new \InvalidArgumentException('$offset should be positive value');
        }

        $this->offset = $offset;

        return $this;
    }

    /**
     * @param QueryBuilder $dql
     *
     * @return Paginator
     */
    public function paginate(QueryBuilder $dql): Paginator
    {
        $paginator = new Paginator($dql);

        $paginator->getQuery()
            ->setFirstResult($this->getLimit() * ($this->getOffset() - 1))
            ->setMaxResults($this->getLimit());

        return $paginator;
    }

    /**
     * Returns repository for entity class name
     *
     * @param string $entityClass
     *
     * @return EntityRepository
     */
    protected function getRepository(string $entityClass): EntityRepository
    {
        return $this->getEntityManager()->getRepository($entityClass);
    }
}
