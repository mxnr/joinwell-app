<?php

namespace JoinWellBundle\Repository;

use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * Class TherapistRepository
 *
 * @package JoinWellBundle\Repository
 */
class TherapistRepository extends AbstractRepository
{
    /**
     * Returns available therapists for requested params
     *
     * @param string $service
     * @param string $language
     * @param string $rate
     *
     * @return Paginator
     */
    public function checkAvailability(string $service, string $language, string $rate): Paginator
    {
        $queryBuilder = $this->createQueryBuilder('t');

        $queryBuilder
            ->leftJoin('t.user', 'u')
            ->leftJoin('u.person', 'p')
            ->andWhere($queryBuilder->expr()->like('t.type', ':service'))
            ->andWhere($queryBuilder->expr()->like('p.spokenLanguages', ':lang'))
            ->andWhere($queryBuilder->expr()->gte('p.rating', ':rating'))
            ->setParameter('service', '%'.$service.'%')
            ->setParameter('lang', '%'.$language.'%')
            ->setParameter('rating', $rate);

        return $this->paginate($queryBuilder);
    }
}
