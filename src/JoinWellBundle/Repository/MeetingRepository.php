<?php

namespace JoinWellBundle\Repository;

use JoinWellBundle\Entity\User;
use JoinWellBundle\Entity\ZoomMeeting;

/**
 * Class MeetingRepository
 * @package JoinWellBundle\Repository
 */
class MeetingRepository extends AbstractRepository
{
    /**
     * @param User $user
     *
     * @return array
     */
    public function getMeetingsHostedBy(User $user)
    {
        $queryBuilder = $this->createQueryBuilder('e');

        $queryBuilder
            ->andWhere($queryBuilder->expr()->eq('e.host', ':user'))
            ->andWhere($queryBuilder->expr()->notIn('e.status', ':status'))
            ->setParameter('status', [ZoomMeeting::STATUS_DONE])
            ->setParameter('user', $user->getId());

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @param User $user
     *
     * @return array
     */
    public function getMeetingsGuestedBy(User $user)
    {
        $queryBuilder = $this->createQueryBuilder('e');

        $queryBuilder
            ->andWhere($queryBuilder->expr()->eq('e.client', ':user'))
            ->andWhere($queryBuilder->expr()->notIn('e.status', ':status'))
            ->setParameter('status', [ZoomMeeting::STATUS_DONE])
            ->setParameter('user', $user->getId());

        return $queryBuilder->getQuery()->getResult();
    }
}
