<?php

namespace JoinWellBundle\Form\Search;

use JoinWellBundle\Entity\Person;
use JoinWellBundle\Entity\Therapist;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class TherapistType
 *
 * @package JoinWellBundle\Form\Search
 */
class TherapistType extends AbstractType
{
    /**
     * Builds therapist "checkAvalability" form
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //TODO: should be injected translation serivce, and keys of types and ratings should be translated!!
        $types = [];

        foreach (Therapist::TYPE_LIST as $type) {
            $types['type_'.$type] = $type;
        }

        $ratings = [];

        foreach (Person::RATING_LIST as $rating) {
            $ratings['rating_'.$rating] = $rating;
        }

        $builder
            ->add(
                'type',
                ChoiceType::class,
                [
                    'choices' => $types,
                ]
            )
            ->add('language', TextType::class)
            ->add(
                'rating',
                ChoiceType::class,
                [
                    'choices' => $ratings,
                ]
            )
            ->add('submit', SubmitType::class);
    }
}
