<?php

namespace JoinWellBundle\Form\Patient;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Class InsuranceInfoType
 * @package JoinWellBundle\Form\Patient
 */
class InsuranceInfoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $providers = [
            '--SELECT PROVIDER--' => null,
            'LOL' => 1,
            'ROFL' => 2,
            'LMFAO' => 3,
        ];

        $providerPlan = [
            '--SELECT PLAN OF SERVICE--' => null,
            'I wanna spend a lots of money!' => 1,
            'I wanna safe my money' => 2,
            'Shut up and take my money!' => 3,
        ];

        //TODO: there should be something
        $builder
            ->add(
                'provider',
                ChoiceType::class,
                [
                    'choices' => $providers,
                ]
            )
            ->add(
                'plan',
                ChoiceType::class,
                [
                    'choices' => $providerPlan,
                ]
            )
            ->add('submit', SubmitType::class);
    }
}
