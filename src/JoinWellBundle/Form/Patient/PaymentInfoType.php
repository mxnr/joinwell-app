<?php

namespace JoinWellBundle\Form\Patient;

use JoinWellBundle\Entity\Embed\CardWallet;
use JoinWellBundle\Entity\Embed\CryptoWallet;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class PaymentInfoType
 * @package JoinWellBundle\Form\Patient
 */
class PaymentInfoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class)
            ->add('lastName', TextType::class)
            ->add(
                $builder->create(
                    'cryptoWallet',
                    FormType::class,
                    [
                        'data_class' => CryptoWallet::class,
                    ]
                )
                    ->add('bitcoinWallet', TextType::class)
                    ->add('ethereumWallet', TextType::class)
            )
            ->add(
                $builder->create(
                    'cardWallet',
                    FormType::class,
                    [
                        'data_class' => CardWallet::class,
                    ]
                )
                    ->add('cardNumber', TextType::class)
                    ->add('cardExpDate', TextType::class)
                    ->add('cardCvv2', NumberType::class)
            )
            ->add('Save', SubmitType::class);
    }
}
