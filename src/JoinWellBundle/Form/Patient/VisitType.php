<?php

namespace JoinWellBundle\Form\Patient;

use JoinWellBundle\Entity\Visit;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


/**
 * Class VisitType
 * @package JoinWellBundle\Form\Patient
 */
class VisitType extends AbstractType
{
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults(
            [
                'episodes' => ['--New--' => null],
            ]
        );
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $typesList = [
            'Phone' => Visit::TYPE_PHONE,
            'Video' => Visit::TYPE_VIDEO,
            'Concierge' => Visit::TYPE_CONCIERGE,
        ];

        $builder
            ->add(
                'episode',
                ChoiceType::class,
                [
                    'choices' => $options['episodes'],
                ]
            )
            ->add(
                'type',
                ChoiceType::class,
                [
                    'choices' => $typesList,
                ]
            )
            ->add('visitAt', DateTimeType::class)
            ->add('submit', SubmitType::class);
    }
}
