<?php

namespace JoinWellBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * Patient
 *
 * @ORM\Table(name="patient", indexes={@ORM\Index(name="fk_patient_contact_info_id_idx",
 *                                                columns={"contact_person_info_id"}),
 * @ORM\Index(name="fk_patient_primary_care_physician_info_idx", columns={"primary_care_physician_info_id"}),
 * @ORM\Index(name="fk_patient_primary_caregiver_info_id_idx", columns={"primary_caregiver_info_id"}),
 * @ORM\Index(name="fk_patient_staffing_coordinator_id_idx", columns={"staffing_coordinator_id"})})
 * @ORM\Entity(repositoryClass="JoinWellBundle\Repository\PatientRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Patient
{
    /**
     * @var string
     *
     * @ORM\Column(name="mrn", type="string", length=45, nullable=true)
     */
    private $mrn;

    /**
     * @var string
     *
     * @ORM\Column(name="advanced_directives", type="string", length=500, nullable=true)
     */
    private $advancedDirectives;

    /**
     * @var string
     *
     * @ORM\Column(name="place_of_residence", type="string", length=100, nullable=true)
     */
    private $placeOfResidence;

    /**
     * @var string
     *
     * @ORM\Column(name="payor_information", type="string", length=100, nullable=true)
     */
    private $payorInformation;

    /**
     * @var string
     *
     * @ORM\Column(name="stripe_customer_id", type="string", length=36, nullable=true)
     */
    private $stripeCustomerId;

    /**
     * @var string
     *
     * @ORM\Column(name="latitude", type="decimal", precision=11, scale=8, nullable=true)
     */
    private $latitude;

    /**
     * @var string
     *
     * @ORM\Column(name="longitude", type="decimal", precision=11, scale=8, nullable=true)
     */
    private $longitude;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var Uuid
     *
     * @ORM\Id
     * @ORM\Column(type="uuid")
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\OneToOne(targetEntity="JoinWellBundle\Entity\User",cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id", unique=true)
     * })
     */
    private $user;

    /**
     * @var \JoinWellBundle\Entity\ContactInfo
     *
     * @ORM\ManyToOne(targetEntity="JoinWellBundle\Entity\ContactInfo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="contact_person_info_id", referencedColumnName="id")
     * })
     */
    private $contactPersonInfo;

    /**
     * @var \JoinWellBundle\Entity\ContactInfo
     *
     * @ORM\ManyToOne(targetEntity="JoinWellBundle\Entity\ContactInfo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="primary_care_physician_info_id", referencedColumnName="id")
     * })
     */
    private $primaryCarePhysicianInfo;

    /**
     * @var \JoinWellBundle\Entity\ContactInfo
     *
     * @ORM\ManyToOne(targetEntity="JoinWellBundle\Entity\ContactInfo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="primary_caregiver_info_id", referencedColumnName="id")
     * })
     */
    private $primaryCaregiverInfo;

    /**
     * @var \JoinWellBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="JoinWellBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="staffing_coordinator_id", referencedColumnName="id")
     * })
     */
    private $staffingCoordinator;

    /**
     * @return string
     */
    public function getMrn()
    {
        return $this->mrn;
    }

    /**
     * @param string $mrn
     *
     * @return Patient
     */
    public function setMrn(string $mrn)
    {
        $this->mrn = $mrn;

        return $this;
    }

    /**
     * @return string
     */
    public function getAdvancedDirectives()
    {
        return $this->advancedDirectives;
    }

    /**
     * @param string $advancedDirectives
     *
     * @return Patient
     */
    public function setAdvancedDirectives($advancedDirectives)
    {
        $this->advancedDirectives = $advancedDirectives;

        return $this;
    }

    /**
     * @return string
     */
    public function getPlaceOfResidence()
    {
        return $this->placeOfResidence;
    }

    /**
     * @param string $placeOfResidence
     *
     * @return Patient
     */
    public function setPlaceOfResidence($placeOfResidence)
    {
        $this->placeOfResidence = $placeOfResidence;

        return $this;
    }

    /**
     * @return string
     */
    public function getPayorInformation()
    {
        return $this->payorInformation;
    }

    /**
     * @param string $payorInformation
     *
     * @return Patient
     */
    public function setPayorInformation($payorInformation)
    {
        $this->payorInformation = $payorInformation;

        return $this;
    }

    /**
     * @return string
     */
    public function getStripeCustomerId()
    {
        return $this->stripeCustomerId;
    }

    /**
     * @param string $stripeCustomerId
     *
     * @return Patient
     */
    public function setStripeCustomerId($stripeCustomerId)
    {
        $this->stripeCustomerId = $stripeCustomerId;

        return $this;
    }

    /**
     * @return string
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param string $latitude
     *
     * @return Patient
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param string $longitude
     *
     * @return Patient
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     *
     * @return Patient
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @ORM\PrePersist()
     */
    public function setCreatedAtWhileCreating()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     *
     * @return Patient
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @ORM\PreUpdate()
     */
    public function setUpdatedAtWhileUpdating()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime $deletedAt
     *
     * @return Patient
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * @return Uuid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     *
     * @return Patient
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return ContactInfo
     */
    public function getContactPersonInfo()
    {
        return $this->contactPersonInfo;
    }

    /**
     * @param ContactInfo $contactPersonInfo
     *
     * @return Patient
     */
    public function setContactPersonInfo($contactPersonInfo)
    {
        $this->contactPersonInfo = $contactPersonInfo;

        return $this;
    }

    /**
     * @return ContactInfo
     */
    public function getPrimaryCarePhysicianInfo()
    {
        return $this->primaryCarePhysicianInfo;
    }

    /**
     * @param ContactInfo $primaryCarePhysicianInfo
     *
     * @return Patient
     */
    public function setPrimaryCarePhysicianInfo($primaryCarePhysicianInfo)
    {
        $this->primaryCarePhysicianInfo = $primaryCarePhysicianInfo;

        return $this;
    }

    /**
     * @return ContactInfo
     */
    public function getPrimaryCaregiverInfo()
    {
        return $this->primaryCaregiverInfo;
    }

    /**
     * @param ContactInfo $primaryCaregiverInfo
     *
     * @return Patient
     */
    public function setPrimaryCaregiverInfo($primaryCaregiverInfo)
    {
        $this->primaryCaregiverInfo = $primaryCaregiverInfo;

        return $this;
    }

    /**
     * @return User
     */
    public function getStaffingCoordinator()
    {
        return $this->staffingCoordinator;
    }

    /**
     * @param User $staffingCoordinator
     *
     * @return Patient
     */
    public function setStaffingCoordinator($staffingCoordinator)
    {
        $this->staffingCoordinator = $staffingCoordinator;

        return $this;
    }
}
