<?php

namespace JoinWellBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * Therapist
 *
 * @ORM\Table(name="therapist", indexes={@ORM\Index(name="fk_therapist_referred_by_idx", columns={"referred_by"})})
 * @ORM\Entity(repositoryClass="JoinWellBundle\Repository\TherapistRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Therapist
{
    const TYPE_PHYSICAL_THERAPIST = 'pt';

    const TYPE_PHYSICAL_THERAPY_ASSISTANT = 'pta';

    const TYPE_SPEECH_LANGUAGE_THERAPIST = 'st';

    const TYPE_SPEECH_LANGUAGE_THERAPIST_ASSISTANT = 'sta';

    const TYPE_NURSE_REGISTERED = 'rn';

    const TYPE_NURSE_LICENSED_VOCATIONAL = 'lvn';

    const TYPE_OCCUPATIONAL_THERAPIST = 'ot';

    const TYPE_OCCUPATIONAL_THERAPIST_AIDE = 'ota';

    const TYPE_OCCUPATIONAL_THERAPIST_ASSISTANT = 'cota';

    const TYPE_HOME_HEALTH_AIDE = 'hha';

    const TYPE_CNA = 'cna';

    /**
     * List of all types that available for entity
     */
    const TYPE_LIST = [
        self::TYPE_PHYSICAL_THERAPIST,
        self::TYPE_PHYSICAL_THERAPY_ASSISTANT,
        self::TYPE_SPEECH_LANGUAGE_THERAPIST,
        self::TYPE_SPEECH_LANGUAGE_THERAPIST_ASSISTANT,
        self::TYPE_NURSE_REGISTERED,
        self::TYPE_NURSE_LICENSED_VOCATIONAL,
        self::TYPE_OCCUPATIONAL_THERAPIST,
        self::TYPE_OCCUPATIONAL_THERAPIST_AIDE,
        self::TYPE_OCCUPATIONAL_THERAPIST_ASSISTANT,
        self::TYPE_HOME_HEALTH_AIDE,
        self::TYPE_CNA,
    ];

    /**
     * @var \Ramsey\Uuid\Uuid
     *
     * @ORM\Id
     * @ORM\Column(type="uuid")
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="experience", type="string", length=1000, nullable=true)
     */
    private $experience;

    /**
     * @var bool
     *
     * @ORM\Column(name="background_check", type="boolean", nullable=true)
     */
    private $backgroundCheck = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="mobile_device", type="string", length=45, nullable=true)
     */
    private $mobileDevice;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=50, nullable=true)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=50, nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="service_area_zip_codes", type="string", length=2000, nullable=true)
     */
    private $serviceAreaZipCodes;

    /**
     * @var string
     *
     * @ORM\Column(name="eval", type="string", length=1000, nullable=true)
     */
    private $eval;

    /**
     * @var string
     *
     * @ORM\Column(name="follow_up", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $followUp;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="activated_at", type="datetime", nullable=true)
     */
    private $activatedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="JoinWellBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="referred_by", referencedColumnName="id")
     * })
     */
    private $referredBy;

    /**
     * @var User
     *
     * @ORM\OneToOne(targetEntity="JoinWellBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id", unique=true)
     * })
     */
    private $user;

    /**
     * @return string
     */
    public function getExperience()
    {
        return $this->experience;
    }

    /**
     * @param string $experience
     *
     * @return Therapist
     */
    public function setExperience($experience)
    {
        $this->experience = $experience;

        return $this;
    }

    /**
     * @return bool
     */
    public function isBackgroundCheck()
    {
        return $this->backgroundCheck;
    }

    /**
     * @param bool $backgroundCheck
     *
     * @return Therapist
     */
    public function setBackgroundCheck($backgroundCheck)
    {
        $this->backgroundCheck = $backgroundCheck;

        return $this;
    }

    /**
     * @return string
     */
    public function getMobileDevice()
    {
        return $this->mobileDevice;
    }

    /**
     * @param string $mobileDevice
     *
     * @return Therapist
     */
    public function setMobileDevice($mobileDevice)
    {
        $this->mobileDevice = $mobileDevice;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     *
     * @return Therapist
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string
     */
    public function getServiceAreaZipCodes()
    {
        return $this->serviceAreaZipCodes;
    }

    /**
     * @param string $serviceAreaZipCodes
     *
     * @return Therapist
     */
    public function setServiceAreaZipCodes($serviceAreaZipCodes)
    {
        $this->serviceAreaZipCodes = $serviceAreaZipCodes;

        return $this;
    }

    /**
     * @return string
     */
    public function getEval()
    {
        return $this->eval;
    }

    /**
     * @param string $eval
     *
     * @return Therapist
     */
    public function setEval($eval)
    {
        $this->eval = $eval;

        return $this;
    }

    /**
     * @return string
     */
    public function getFollowUp()
    {
        return $this->followUp;
    }

    /**
     * @param string $followUp
     *
     * @return Therapist
     */
    public function setFollowUp($followUp)
    {
        $this->followUp = $followUp;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getActivatedAt()
    {
        return $this->activatedAt;
    }

    /**
     * @param \DateTime $activatedAt
     *
     * @return Therapist
     */
    public function setActivatedAt($activatedAt)
    {
        $this->activatedAt = $activatedAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     *
     * @return Therapist
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @ORM\PrePersist()
     */
    public function setCreatedAtWhileCreating()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     *
     * @return Therapist
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @ORM\PreUpdate()
     */
    public function setUpdatedAtWhileUpdating()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime $deletedAt
     *
     * @return Therapist
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * @return string
     */
    public function getReferredBy()
    {
        return $this->referredBy;
    }

    /**
     * @param string $referredBy
     *
     * @return Therapist
     */
    public function setReferredBy($referredBy)
    {
        $this->referredBy = $referredBy;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return Therapist
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return Uuid
     */
    public function getId()
    {
        return $this->id;
    }
}
