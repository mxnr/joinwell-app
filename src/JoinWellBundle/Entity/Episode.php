<?php

namespace JoinWellBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Episode
 *
 * @ORM\Table(name="episode", indexes={@ORM\Index(name="patient_id_idx", columns={"patient_id"}),
 *     @ORM\Index(name="status", columns={"status"})})
 * @ORM\Entity(repositoryClass="JoinWellBundle\Repository\EpisodeRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Episode
{
    const STATUS_NEW = 0;

    const STATUS_WAITING_FOR_VISIT = 1;

    const STATUS_WAIT_FOR_REVISIT = 2;

    const STATUS_DIAGNOSIS_IS_UNDER_THE_QUESTION = 3;

    const STATUS_SOLVED = 100;

    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(type="uuid")
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="text", length=255, nullable=false)
     */
    private $subject;

    /**
     * @var string
     *
     * @ORM\Column(name="anamnesis", type="text", length=65535, nullable=true)
     */
    private $anamnesis;

    /**
     * @var string
     *
     * @ORM\Column(name="primary_diagnosis", type="text", length=65535, nullable=true)
     */
    private $primaryDiagnosis;

    /**
     * @var string
     *
     * @ORM\Column(name="secondary_diagnosis", type="text", length=65535, nullable=true)
     */
    private $secondaryDiagnosis;

    /**
     * @var string
     *
     * @ORM\Column(name="additional_information", type="text", length=65535, nullable=true)
     */
    private $additionalInformation;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_therapy_needed", type="boolean", nullable=true)
     */
    private $isTherapyNeeded;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt = 'CURRENT_TIMESTAMP';

    /**
     * @var DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var \JoinWellBundle\Entity\Patient
     *
     * @ORM\ManyToOne(targetEntity="JoinWellBundle\Entity\Patient")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="patient_id", referencedColumnName="id")
     * })
     */
    private $patient;

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     *
     * @return Episode
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return Episode
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     *
     * @return Episode
     */
    public function setStatus($status)
    {
        $statusVariantsList = [
            self::STATUS_NEW,
            self::STATUS_WAIT_FOR_REVISIT,
            self::STATUS_DIAGNOSIS_IS_UNDER_THE_QUESTION,
            self::STATUS_SOLVED,
        ];

        if (in_array($status, $statusVariantsList) === false) {
            throw new \InvalidArgumentException(
                sprintf(
                    '$status must be on of [ %s ] values',
                    implode(', ', $statusVariantsList)
                )
            );
        }

        $this->status = $status;

        return $this;
    }

    /**
     * @return string
     */
    public function getAnamnesis()
    {
        return $this->anamnesis;
    }

    /**
     * @param string $anamnesis
     *
     * @return Episode
     */
    public function setAnamnesis($anamnesis)
    {
        $this->anamnesis = $anamnesis;

        return $this;
    }

    /**
     * @return string
     */
    public function getPrimaryDiagnosis()
    {
        return $this->primaryDiagnosis;
    }

    /**
     * @param string $primaryDiagnosis
     *
     * @return Episode
     */
    public function setPrimaryDiagnosis($primaryDiagnosis)
    {
        $this->primaryDiagnosis = $primaryDiagnosis;

        return $this;
    }

    /**
     * @return string
     */
    public function getSecondaryDiagnosis()
    {
        return $this->secondaryDiagnosis;
    }

    /**
     * @param string $secondaryDiagnosis
     *
     * @return Episode
     */
    public function setSecondaryDiagnosis($secondaryDiagnosis)
    {
        $this->secondaryDiagnosis = $secondaryDiagnosis;

        return $this;
    }

    /**
     * @return string
     */
    public function getAdditionalInformation()
    {
        return $this->additionalInformation;
    }

    /**
     * @param string $additionalInformation
     *
     * @return Episode
     */
    public function setAdditionalInformation($additionalInformation)
    {
        $this->additionalInformation = $additionalInformation;

        return $this;
    }

    /**
     * @return bool
     */
    public function isTherapyNeeded()
    {
        return $this->isTherapyNeeded;
    }

    /**
     * @param bool $isTherapyNeeded
     *
     * @return Episode
     */
    public function setIsTherapyNeeded($isTherapyNeeded)
    {
        $this->isTherapyNeeded = $isTherapyNeeded;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     *
     * @return Episode
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @ORM\PrePersist()
     */
    public function setCreatedAtWhileCreating()
    {
        $this->createdAt = new DateTime();
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTime $updatedAt
     *
     * @return Episode
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @ORM\PreUpdate()
     */
    public function setUpdateAtWhileUpdating()
    {
        $this->updatedAt = new DateTime();
    }

    /**
     * @return DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param DateTime $deletedAt
     *
     * @return Episode
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * @return Patient
     */
    public function getPatient()
    {
        return $this->patient;
    }

    /**
     * @param Patient $patient
     *
     * @return Episode
     */
    public function setPatient($patient)
    {
        $this->patient = $patient;

        return $this;
    }
}
