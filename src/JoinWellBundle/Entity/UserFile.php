<?php

namespace JoinWellBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * UserFile
 *
 * @ORM\Table(name="user_file", indexes={@ORM\Index(name="fk_user_file_created_by_idx", columns={"created_by"})})
 * @ORM\Entity
 */
class UserFile
{
    /**
     * @var Uuid
     *
     * @ORM\Id
     * @ORM\Column(type="uuid")
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="file_name", type="string", length=250, nullable=true)
     */
    private $fileName;

    /**
     * @var string
     *
     * @ORM\Column(name="cloudmine_id", type="string", length=36, nullable=true)
     */
    private $cloudmineId;

    /**
     * @var string
     *
     * @ORM\Column(name="cloudmine_file_name", type="string", length=60, nullable=true)
     */
    private $cloudmineFileName;

    /**
     * @var string
     *
     * @ORM\Column(name="reference_id", type="string", length=36, nullable=true)
     */
    private $referenceId;

    /**
     * @var string
     *
     * @ORM\Column(name="reference_type", type="string", nullable=true)
     */
    private $referenceType;

    /**
     * @var string
     *
     * @ORM\Column(name="doc_name", type="string", length=200, nullable=true)
     */
    private $docName;

    /**
     * @var string
     *
     * @ORM\Column(name="doc_description", type="string", length=200, nullable=true)
     */
    private $docDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="aws_file_key", type="string", length=1000, nullable=true)
     */
    private $awsFileKey;

    /**
     * @var string
     *
     * @ORM\Column(name="visibility", type="string", length=45, nullable=true)
     */
    private $visibility;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expiration_date", type="datetime", nullable=true)
     */
    private $expirationDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var \JoinWellBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="JoinWellBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     * })
     */
    private $createdBy;

    /**
     * @return Uuid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param string $fileName
     *
     * @return UserFile
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * @return string
     */
    public function getCloudmineId()
    {
        return $this->cloudmineId;
    }

    /**
     * @param string $cloudmineId
     *
     * @return UserFile
     */
    public function setCloudmineId($cloudmineId)
    {
        $this->cloudmineId = $cloudmineId;

        return $this;
    }

    /**
     * @return string
     */
    public function getCloudmineFileName()
    {
        return $this->cloudmineFileName;
    }

    /**
     * @param string $cloudmineFileName
     *
     * @return UserFile
     */
    public function setCloudmineFileName($cloudmineFileName)
    {
        $this->cloudmineFileName = $cloudmineFileName;

        return $this;
    }

    /**
     * @return string
     */
    public function getReferenceId()
    {
        return $this->referenceId;
    }

    /**
     * @param string $referenceId
     *
     * @return UserFile
     */
    public function setReferenceId($referenceId)
    {
        $this->referenceId = $referenceId;

        return $this;
    }

    /**
     * @return string
     */
    public function getReferenceType()
    {
        return $this->referenceType;
    }

    /**
     * @param string $referenceType
     *
     * @return UserFile
     */
    public function setReferenceType($referenceType)
    {
        $this->referenceType = $referenceType;

        return $this;
    }

    /**
     * @return string
     */
    public function getDocName()
    {
        return $this->docName;
    }

    /**
     * @param string $docName
     *
     * @return UserFile
     */
    public function setDocName($docName)
    {
        $this->docName = $docName;

        return $this;
    }

    /**
     * @return string
     */
    public function getDocDescription()
    {
        return $this->docDescription;
    }

    /**
     * @param string $docDescription
     *
     * @return UserFile
     */
    public function setDocDescription($docDescription)
    {
        $this->docDescription = $docDescription;

        return $this;
    }

    /**
     * @return string
     */
    public function getAwsFileKey()
    {
        return $this->awsFileKey;
    }

    /**
     * @param string $awsFileKey
     *
     * @return UserFile
     */
    public function setAwsFileKey($awsFileKey)
    {
        $this->awsFileKey = $awsFileKey;

        return $this;
    }

    /**
     * @return string
     */
    public function getVisibility()
    {
        return $this->visibility;
    }

    /**
     * @param string $visibility
     *
     * @return UserFile
     */
    public function setVisibility($visibility)
    {
        $this->visibility = $visibility;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getExpirationDate()
    {
        return $this->expirationDate;
    }

    /**
     * @param \DateTime $expirationDate
     *
     * @return UserFile
     */
    public function setExpirationDate($expirationDate)
    {
        $this->expirationDate = $expirationDate;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     *
     * @return UserFile
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     *
     * @return UserFile
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime $deletedAt
     *
     * @return UserFile
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param User $createdBy
     *
     * @return UserFile
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }
}
