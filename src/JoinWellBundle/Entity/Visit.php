<?php

namespace JoinWellBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Visit
 *
 * @ORM\Table(name="visit", indexes={@ORM\Index(name="episode_idx", columns={"episode_id"}),
 *     @ORM\Index(name="therapist_id_idx", columns={"therapist_id"}), @ORM\Index(name="date", columns={"visit_at",
 *     "status"}), @ORM\Index(name="status", columns={"status", "visit_at"})})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class Visit
{
    const TYPE_PHONE = 0;

    const TYPE_VIDEO = 1;

    const TYPE_CONCIERGE = 2;

    const STATUS_NEW = 0;

    const STATUS_SCHEDULED = 1;

    const STATUS_DONE = 10;

    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(type="uuid")
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="integer", nullable=false)
     */
    private $type = '0';

    /**
     * @var DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt = 'CURRENT_TIMESTAMP';

    /**
     * @var DateTime
     *
     * @ORM\Column(name="visit_at", type="datetime", nullable=false)
     * @Assert\GreaterThanOrEqual("+30 minutes")
     */
    private $visitAt;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var \JoinWellBundle\Entity\Episode
     *
     * @ORM\ManyToOne(targetEntity="JoinWellBundle\Entity\Episode",cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="episode_id", referencedColumnName="id")
     * })
     */
    private $episode;

    /**
     * @var \JoinWellBundle\Entity\Therapist
     *
     * @ORM\ManyToOne(targetEntity="JoinWellBundle\Entity\Therapist")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="therapist_id", referencedColumnName="id")
     * })
     */
    private $therapist;

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     *
     * @return Visit
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return Visit
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     *
     * @return Visit
     */
    public function setStatus($status)
    {
        $statusVariantsList = [
            self::STATUS_NEW,
            self::STATUS_SCHEDULED,
            self::STATUS_DONE,
        ];

        if (in_array($status, $statusVariantsList) === false) {
            throw new \InvalidArgumentException(
                sprintf(
                    '$status must be one of [ %s ] values',
                    implode(', ', $statusVariantsList)
                )
            );
        }

        $this->status = $status;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     *
     * @return Visit
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @ORM\PrePersist()
     */
    public function setCreatedAtWhileCreating()
    {
        $this->createdAt = new DateTime();
    }

    /**
     * @return DateTime
     */
    public function getVisitAt()
    {
        return $this->visitAt;
    }

    /**
     * @param DateTime $visitAt
     *
     * @return Visit
     */
    public function setVisitAt($visitAt)
    {
        $this->visitAt = $visitAt;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTime $updatedAt
     *
     * @return Visit
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @ORM\PreUpdate()
     */
    public function setUpdatedAtWhileUpdating()
    {
        $this->updatedAt = new DateTime();
    }

    /**
     * @return DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param DateTime $deletedAt
     *
     * @return Visit
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * @return Episode
     */
    public function getEpisode()
    {
        return $this->episode;
    }

    /**
     * @param Episode $episode
     *
     * @return Visit
     */
    public function setEpisode($episode)
    {
        $this->episode = $episode;

        return $this;
    }

    /**
     * @return Therapist
     */
    public function getTherapist()
    {
        return $this->therapist;
    }

    /**
     * @param Therapist $therapist
     *
     * @return Visit
     */
    public function setTherapist($therapist)
    {
        $this->therapist = $therapist;

        return $this;
    }
}
