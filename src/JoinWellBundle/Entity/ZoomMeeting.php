<?php

namespace JoinWellBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * ZoomMeeting
 *
 * @ORM\Table(name="zoom_meeting", indexes={@ORM\Index(name="host_idx", columns={"host_id"}),
 *     @ORM\Index(name="client_idx", columns={"client_id"})})
 * @ORM\Entity(repositoryClass="JoinWellBundle\Repository\MeetingRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ZoomMeeting
{
    const STATUS_NEW = 0;

    const STATUS_SCHEDULED = 1;

    const STATUS_DONE = 10;

    /**
     * @var Uuid
     *
     * @ORM\Id
     * @ORM\Column(type="uuid")
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="zoom_id", type="string", length=100, nullable=false)
     */
    private $zoomId;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="scheduled_at", type="datetime", nullable=false)
     */
    private $scheduledAt;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status = 0;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var \JoinWellBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="JoinWellBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="host_id", referencedColumnName="id")
     * })
     */
    private $host;

    /**
     * @var \JoinWellBundle\Entity\Visit
     *
     * @ORM\ManyToOne(targetEntity="JoinWellBundle\Entity\Visit", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="visit_id", referencedColumnName="id")
     * })
     */
    private $visit;

    /**
     * @var string
     *
     * @ORM\Column(name="host_url", type="string", length=1000, nullable=false)
     */
    private $hostUrl;

    /**
     * @var \JoinWellBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="JoinWellBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     * })
     */
    private $client;

    /**
     * @var string
     *
     * @ORM\Column(name="client_url", type="string", length=1000, nullable=false)
     */
    private $clientUrl;

    /**
     * @return Visit
     */
    public function getVisit(): Visit
    {
        return $this->visit;
    }

    /**
     * @return bool
     */
    public function hasVisit(): bool
    {
        return !empty($this->visit);
    }

    /**
     * @param Visit $visit
     *
     * @return ZoomMeeting
     */
    public function setVisit(Visit $visit): ZoomMeeting
    {
        $this->visit = $visit;

        return $this;
    }

    /**
     * @return string
     */
    public function getHostUrl(): string
    {
        return $this->hostUrl;
    }

    /**
     * @return bool
     */
    public function hasHostUrl(): bool
    {
        return !empty($this->hostUrl);
    }

    /**
     * @param string $hostUrl
     *
     * @return ZoomMeeting
     */
    public function setHostUrl(string $hostUrl): ZoomMeeting
    {
        $this->hostUrl = $hostUrl;

        return $this;
    }

    /**
     * @return string
     */
    public function getClientUrl(): string
    {
        return $this->clientUrl;
    }

    /**
     * @return bool
     */
    public function hasClientUrl(): bool
    {
        return !empty($this->clientUrl);
    }

    /**
     * @param string $clientUrl
     *
     * @return ZoomMeeting
     */
    public function setClientUrl(string $clientUrl): ZoomMeeting
    {
        $this->clientUrl = $clientUrl;

        return $this;
    }

    /**
     * @return string
     */
    public function getZoomId(): string
    {
        return $this->zoomId;
    }

    /**
     * @return bool
     */
    public function hasZoomId(): bool
    {
        return !empty($this->zoomId);
    }

    /**
     * @param string $zoomId
     *
     * @return ZoomMeeting
     */
    public function setZoomId(string $zoomId): ZoomMeeting
    {
        $this->zoomId = $zoomId;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @return bool
     */
    public function hasStatus(): bool
    {
        return !empty($this->status);
    }

    /**
     * @param int $status
     *
     * @return ZoomMeeting
     */
    public function setStatus(int $status)
    {
        $statusVariantsList = [
            self::STATUS_NEW,
            self::STATUS_SCHEDULED,
            self::STATUS_DONE,
        ];

        if (in_array($status, $statusVariantsList) === false) {
            throw  new \InvalidArgumentException(
                sprintf(
                    '$status must be on of [ %s ] values',
                    implode(', ', $statusVariantsList)
                )
            );
        }
        $this->status = $status;

        return $this;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function hasId(): bool
    {
        return !empty($this->id);
    }

    /**
     * @param string $id
     *
     * @return ZoomMeeting
     */
    public function setId(string $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return bool
     */
    public function hasCreatedAt(): bool
    {
        return !empty($this->createdAt);
    }

    /**
     * @param DateTime $createdAt
     *
     * @return ZoomMeeting
     */
    public function setCreatedAt(DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @ORM\PrePersist()
     */
    public function setCreatedAtWhileCreating()
    {
        $this->createdAt = new DateTime();
    }

    /**
     * @return DateTime
     */
    public function getScheduledAt(): DateTime
    {
        return $this->scheduledAt;
    }

    /**
     * @return bool
     */
    public function hasScheduledAt(): bool
    {
        return !empty($this->scheduledAt);
    }

    /**
     * @param DateTime $scheduledAt
     *
     * @return ZoomMeeting
     */
    public function setScheduledAt(DateTime $scheduledAt)
    {
        $this->scheduledAt = $scheduledAt;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt(): DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @return bool
     */
    public function hasUpdatedAt(): bool
    {
        return !empty($this->updatedAt);
    }

    /**
     * @param DateTime $updatedAt
     *
     * @return ZoomMeeting
     */
    public function setUpdatedAt(DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @ORM\PreUpdate()
     */
    public function setUpdatedAtWhileUpdating()
    {
        $this->updatedAt = new DateTime();
    }

    /**
     * @return DateTime
     */
    public function getDeletedAt(): DateTime
    {
        return $this->deletedAt;
    }

    /**
     * @return bool
     */
    public function hasDeletedAt(): bool
    {
        return !empty($this->deletedAt);
    }

    /**
     * @param DateTime $deletedAt
     *
     * @return ZoomMeeting
     */
    public function setDeletedAt(DateTime $deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * @return User
     */
    public function getHost(): User
    {
        return $this->host;
    }

    /**
     * @return bool
     */
    public function hasHost(): bool
    {
        return !empty($this->host);
    }

    /**
     * @param User $host
     *
     * @return ZoomMeeting
     */
    public function setHost(User $host)
    {
        $this->host = $host;

        return $this;
    }

    /**
     * @return User
     */
    public function getClient(): User
    {
        return $this->client;
    }

    /**
     * @return bool
     */
    public function hasClient(): bool
    {
        return !empty($this->client);
    }

    /**
     * @param User $client
     *
     * @return ZoomMeeting
     */
    public function setClient(User $client)
    {
        $this->client = $client;

        return $this;
    }
}
