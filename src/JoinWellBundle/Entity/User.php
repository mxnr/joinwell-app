<?php

namespace JoinWellBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Ramsey\Uuid\Uuid;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * User
 *
 * @ORM\Table(name="`user`", uniqueConstraints={
 *  @ORM\UniqueConstraint(name="person_id_UNIQUE", columns={"person_id"}),
 *  @ORM\UniqueConstraint(name="email_UNIQUE", columns={"email"}),
 *  @ORM\UniqueConstraint(name="UNIQ_9D83D648A1D16FBF", columns={"username"}),
 *  @ORM\UniqueConstraint(name="zoom_id_UNIQUE", columns={"zoom_id"}),
 *  @ORM\UniqueConstraint(
 *     name="cloudmine_id_UNIQUE", columns={"cloudmine_id"})},
 *     indexes={@ORM\Index(name="fk_user_idx", columns={"person_id"}
 *  ),
 *  @ORM\Index(name="fk_user_contact_info_id_idx", columns={"contact_info_id"}),
 *  @ORM\Index(name="fk_user_address_info_id_idx", columns={"address_info_id"})
 * })
 * @UniqueEntity("email")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class User extends BaseUser
{
    const TYPE_THERAPIST = 'Therapist';

    const TYPE_PATIENT = 'Patient';

    const TYPE_WELL_ADMIN = 'WellAdmin';

    const TYPE_WELL_USER = 'WellUser';

    const TYPE_ORGANIZATION_ADMIN = 'OrgAdmin';

    const TYPE_ORGANIZATION_USER = 'OrgUser';

    /**
     * @var Uuid
     *
     * @ORM\Id
     * @ORM\Column(type="uuid")
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="cloudmine_id", type="string", length=36, nullable=true)
     */
    private $cloudmineId;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=45, nullable=true)
     */
    private $type;

    /**
     * @var bool
     *
     * @ORM\Column(name="email_notifications", type="boolean", nullable=true)
     */
    private $emailNotifications;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var \JoinWellBundle\Entity\AddressInfo
     *
     * @ORM\ManyToOne(targetEntity="JoinWellBundle\Entity\AddressInfo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="address_info_id", referencedColumnName="id")
     * })
     */
    private $addressInfo;

    /**
     * @var \JoinWellBundle\Entity\ContactInfo
     *
     * @ORM\ManyToOne(targetEntity="JoinWellBundle\Entity\ContactInfo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="contact_info_id", referencedColumnName="id")
     * })
     */
    private $contactInfo;

    /**
     * @var \JoinWellBundle\Entity\Person
     *
     * @ORM\OneToOne(targetEntity="JoinWellBundle\Entity\Person",cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="person_id", referencedColumnName="id")
     * })
     */
    private $person;

    /**
     * @var string
     *
     * @ORM\Column(name="zoom_id", type="string", length=100, nullable=true, unique=true)
     */
    private $zoomId;

    /**
     * @return string
     */
    public function getZoomId(): string
    {
        return $this->zoomId;
    }

    /**
     * @return bool
     */
    public function hasZoomId(): bool
    {
        return !empty($this->zoomId);
    }

    /**
     * @param string $zoomId
     *
     * @return User
     */
    public function setZoomId(string $zoomId)
    {
        $this->zoomId = $zoomId;

        return $this;
    }

    /**
     * @ORM\PrePersist()
     */
    public function createUsernameWhileCreating()
    {
        if (empty($this->username) && !empty($this->email)) {
            $this->username = $this->email;
        }
    }

    /**
     * @return string
     */
    public function getCloudmineId()
    {
        return $this->cloudmineId;
    }

    /**
     * @param string $cloudmineId
     *
     * @return User
     */
    public function setCloudmineId($cloudmineId)
    {
        $this->cloudmineId = $cloudmineId;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return User
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEmailNotifications()
    {
        return $this->emailNotifications;
    }

    /**
     * @param bool $emailNotifications
     *
     * @return User
     */
    public function setEmailNotifications($emailNotifications)
    {
        $this->emailNotifications = $emailNotifications;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     *
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @ORM\PrePersist()
     */
    public function setCreatedAtWhileCreating()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     *
     * @return User
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @ORM\PreUpdate()
     */
    public function setUpdatedAtWhileUpdating()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime $deletedAt
     *
     * @return User
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * @return AddressInfo
     */
    public function getAddressInfo()
    {
        return $this->addressInfo;
    }

    /**
     * @param AddressInfo $addressInfo
     *
     * @return User
     */
    public function setAddressInfo($addressInfo)
    {
        $this->addressInfo = $addressInfo;

        return $this;
    }

    /**
     * @return ContactInfo
     */
    public function getContactInfo()
    {
        return $this->contactInfo;
    }

    /**
     * @param ContactInfo $contactInfo
     *
     * @return User
     */
    public function setContactInfo($contactInfo)
    {
        $this->contactInfo = $contactInfo;

        return $this;
    }

    /**
     * @return Person
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * @param Person $person
     *
     * @return User
     */
    public function setPerson($person)
    {
        $this->person = $person;

        return $this;
    }
}
