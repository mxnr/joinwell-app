<?php

namespace JoinWellBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * TherapistAvailability
 *
 * @ORM\Table(name="therapist_availability", indexes={@ORM\Index(name="fk_theavail_therapist_id_idx",
 *     columns={"therapist_id"})})
 * @ORM\Entity
 */
class TherapistAvailability
{
    /**
     * @var Uuid
     *
     * @ORM\Id
     * @ORM\Column(type="uuid")
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="sunday", type="string", length=10, nullable=true)
     */
    private $sunday;

    /**
     * @var string
     *
     * @ORM\Column(name="monday", type="string", length=10, nullable=true)
     */
    private $monday;

    /**
     * @var string
     *
     * @ORM\Column(name="tuesday", type="string", length=10, nullable=true)
     */
    private $tuesday;

    /**
     * @var string
     *
     * @ORM\Column(name="wednesday", type="string", length=10, nullable=true)
     */
    private $wednesday;

    /**
     * @var string
     *
     * @ORM\Column(name="thursday", type="string", length=10, nullable=true)
     */
    private $thursday;

    /**
     * @var string
     *
     * @ORM\Column(name="friday", type="string", length=10, nullable=true)
     */
    private $friday;

    /**
     * @var string
     *
     * @ORM\Column(name="saturday", type="string", length=10, nullable=true)
     */
    private $saturday;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var \JoinWellBundle\Entity\Therapist
     *
     * @ORM\ManyToOne(targetEntity="JoinWellBundle\Entity\Therapist")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="therapist_id", referencedColumnName="id")
     * })
     */
    private $therapist;

    /**
     * @return Uuid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSunday()
    {
        return $this->sunday;
    }

    /**
     * @param string $sunday
     *
     * @return TherapistAvailability
     */
    public function setSunday($sunday)
    {
        $this->sunday = $sunday;

        return $this;
    }

    /**
     * @return string
     */
    public function getMonday()
    {
        return $this->monday;
    }

    /**
     * @param string $monday
     *
     * @return TherapistAvailability
     */
    public function setMonday($monday)
    {
        $this->monday = $monday;

        return $this;
    }

    /**
     * @return string
     */
    public function getTuesday()
    {
        return $this->tuesday;
    }

    /**
     * @param string $tuesday
     *
     * @return TherapistAvailability
     */
    public function setTuesday($tuesday)
    {
        $this->tuesday = $tuesday;

        return $this;
    }

    /**
     * @return string
     */
    public function getWednesday()
    {
        return $this->wednesday;
    }

    /**
     * @param string $wednesday
     *
     * @return TherapistAvailability
     */
    public function setWednesday($wednesday)
    {
        $this->wednesday = $wednesday;

        return $this;
    }

    /**
     * @return string
     */
    public function getThursday()
    {
        return $this->thursday;
    }

    /**
     * @param string $thursday
     *
     * @return TherapistAvailability
     */
    public function setThursday($thursday)
    {
        $this->thursday = $thursday;

        return $this;
    }

    /**
     * @return string
     */
    public function getFriday()
    {
        return $this->friday;
    }

    /**
     * @param string $friday
     *
     * @return TherapistAvailability
     */
    public function setFriday($friday)
    {
        $this->friday = $friday;

        return $this;
    }

    /**
     * @return string
     */
    public function getSaturday()
    {
        return $this->saturday;
    }

    /**
     * @param string $saturday
     *
     * @return TherapistAvailability
     */
    public function setSaturday($saturday)
    {
        $this->saturday = $saturday;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     *
     * @return TherapistAvailability
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     *
     * @return TherapistAvailability
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime $deletedAt
     *
     * @return TherapistAvailability
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * @return Therapist
     */
    public function getTherapist()
    {
        return $this->therapist;
    }

    /**
     * @param Therapist $therapist
     *
     * @return TherapistAvailability
     */
    public function setTherapist($therapist)
    {
        $this->therapist = $therapist;

        return $this;
    }
}
