<?php

namespace JoinWellBundle\Entity\Embed;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\AbstractType;

/**
 * Class InsuranceInfo
 * @package JoinWellBundle\Entity\Embed
 */
class InsuranceInfo extends AbstractType
{
    /**
     * @Assert\NotBlank(message="Please choose provider from the list")
     */
    private $provider;

    /**
     * @Assert\NotBlank(message="Please choose provider plan from the list")
     */
    private $plan;

    /**
     * @return mixed
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * @param mixed $provider
     *
     * @return InsuranceInfo
     */
    public function setProvider($provider)
    {
        $this->provider = $provider;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * @param mixed $plan
     *
     * @return InsuranceInfo
     */
    public function setPlan($plan)
    {
        $this->plan = $plan;

        return $this;
    }
}
