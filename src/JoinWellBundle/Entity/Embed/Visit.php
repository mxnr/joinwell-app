<?php

namespace JoinWellBundle\Entity\Embed;

use Symfony\Component\Validator\Constraints as Assert;
/**
 * Class Visit
 *
 * @package JoinWellBundle\Entity\Embed
 */
class Visit
{
    private $type;

    /**
     * @Assert\DateTime()
     */
    private $dateTime;

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     *
     * @return Visit
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateTime()
    {
        return $this->dateTime;
    }

    /**
     * @param mixed $dateTime
     *
     * @return Visit
     */
    public function setDateTime($dateTime)
    {
        $this->dateTime = $dateTime;

        return $this;
    }
}
