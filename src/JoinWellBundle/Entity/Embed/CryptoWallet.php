<?php

namespace JoinWellBundle\Entity\Embed;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class CryptoWallet
 * @package JoinWellBundle\Entity\Embed
 */
class CryptoWallet
{
    /**
     * @Assert\Regex(
     *     pattern="/^[13][a-km-zA-HJ-NP-Z1-9]{25,34}$/",
     *     message="Please provide valid address of the wallet"
     * )
     */
    private $bitcoinWallet;

    /**
     * @Assert\Regex(
     *     pattern="/^0x[a-fA-F0-9]{40}$/",
     *     message="Please provide valid address of the wallet"
     * )
     */
    private $ethereumWallet;

    /**
     * @return mixed
     */
    public function getEthereumWallet()
    {
        return $this->ethereumWallet;
    }

    /**
     * @param mixed $ethereumWallet
     *
     * @return CryptoWallet
     */
    public function setEthereumWallet($ethereumWallet)
    {
        $this->ethereumWallet = $ethereumWallet;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getBitcoinWallet()
    {
        return $this->bitcoinWallet;
    }

    /**
     * @param mixed $bitcoinWallet
     *
     * @return CryptoWallet
     */
    public function setBitcoinWallet($bitcoinWallet)
    {
        $this->bitcoinWallet = $bitcoinWallet;

        return $this;
    }

}
