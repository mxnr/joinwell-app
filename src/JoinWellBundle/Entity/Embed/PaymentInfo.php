<?php

namespace JoinWellBundle\Entity\Embed;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class PaymentInfo
 * @package JoinWellBundle\Entity\Embed
 */
class PaymentInfo extends AbstractType
{
    private $firstName;

    private $lastName;

    /**
     * @Assert\Regex(
     *     pattern="/^[13][a-km-zA-HJ-NP-Z1-9]{25,34}$/",
     *     message="Please provide valid address of the wallet"
     * )
     */
    private $bitcoinWallet;

    private $etheriumWallet;

    /**
     * @Assert\CardScheme(schemes={"VISA","MASTERCARD","AMEX"}, message="Please provide valid card number")
     */
    private $cardNumber;

    /**
     * @Assert\Regex(pattern="/^(0[1-9]|10|11|12)\/([0-9]{2})$/", message="Please provide valid expiration date")
     */
    private $cardExpDate;

    /**
     * @Assert\LessThanOrEqual(999, message="Cvv2 code should be 3 digit number")
     * @Assert\GreaterThanOrEqual(100,message="Cvv2 code should be 3 digit number")
     */
    private $cardCvv2;

    /**
     * @Assert\Valid
     */
    private $cryptoWallets;

    /**
     * @Assert\Valid
     */
    private $cardWallet;

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     *
     * @return PaymentInfo
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     *
     * @return PaymentInfo
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCardWallet()
    {
        return $this->cardWallet;
    }

    /**
     * @param mixed $cardWallet
     *
     * @return PaymentInfo
     */
    public function setCardWallet($cardWallet)
    {
        $this->cardWallet = $cardWallet;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCryptoWallets()
    {
        return $this->cryptoWallets;
    }

    /**
     * @param mixed $cryptoWallets
     *
     * @return PaymentInfo
     */
    public function setCryptoWallets($cryptoWallets)
    {
        $this->cryptoWallets = $cryptoWallets;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getBitcoinWallet()
    {
        return $this->bitcoinWallet;
    }

    /**
     * @param mixed $bitcoinWallet
     *
     * @return PaymentInfo
     */
    public function setBitcoinWallet($bitcoinWallet)
    {
        $this->bitcoinWallet = $bitcoinWallet;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEtheriumWallet()
    {
        return $this->etheriumWallet;
    }

    /**
     * @param mixed $etheriumWallet
     *
     * @return PaymentInfo
     */
    public function setEtheriumWallet($etheriumWallet)
    {
        $this->etheriumWallet = $etheriumWallet;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCardNumber()
    {
        return $this->cardNumber;
    }

    /**
     * @param mixed $cardNumber
     *
     * @return PaymentInfo
     */
    public function setCardNumber($cardNumber)
    {
        $this->cardNumber = $cardNumber;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCardExpDate()
    {
        return $this->cardExpDate;
    }

    /**
     * @param mixed $cardExpDate
     *
     * @return PaymentInfo
     */
    public function setCardExpDate($cardExpDate)
    {
        $this->cardExpDate = $cardExpDate;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCardCvv2()
    {
        return $this->cardCvv2;
    }

    /**
     * @param mixed $cardCvv2
     *
     * @return PaymentInfo
     */
    public function setCardCvv2($cardCvv2)
    {
        $this->cardCvv2 = $cardCvv2;

        return $this;
    }
}
