<?php

namespace JoinWellBundle\Entity\Embed;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class CardWallet
 * @package JoinWellBundle\Entity\Embed
 */
class CardWallet
{
    /**
     * @Assert\CardScheme(schemes={"VISA","MASTERCARD","AMEX"}, message="Please provide valid card number")
     */
    private $cardNumber;

    /**
     * @Assert\Regex(pattern="/^(0[1-9]|10|11|12)\/([0-9]{2})$/", message="Please provide valid expiration date")
     */
    private $cardExpDate;

    /**
     * @Assert\LessThanOrEqual(999, message="Cvv2 code should be 3 digit number")
     * @Assert\GreaterThanOrEqual(100,message="Cvv2 code should be 3 digit number")
     */
    private $cardCvv2;

    /**
     * @return mixed
     */
    public function getCardNumber()
    {
        return $this->cardNumber;
    }

    /**
     * @param mixed $cardNumber
     *
     * @return CardWallet
     */
    public function setCardNumber($cardNumber)
    {
        $this->cardNumber = $cardNumber;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCardExpDate()
    {
        return $this->cardExpDate;
    }

    /**
     * @param mixed $cardExpDate
     *
     * @return CardWallet
     */
    public function setCardExpDate($cardExpDate)
    {
        $this->cardExpDate = $cardExpDate;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCardCvv2()
    {
        return $this->cardCvv2;
    }

    /**
     * @param mixed $cardCvv2
     *
     * @return CardWallet
     */
    public function setCardCvv2($cardCvv2)
    {
        $this->cardCvv2 = $cardCvv2;

        return $this;
    }
}
