<?php

namespace JoinWellBundle\Entity\Embed;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Class User
 *
 * @package JoinWellBundle\Entity\Embed
 *
 * @ORM\Table(name="user")
 * @ORM\Entity()
 * @UniqueEntity("email")
 */
class User
{
    /**
     * @Assert\Email()
     * @ORM\Column(name="email", type="string", nullable=false)
     */
    protected $email;

    /**
     * @Assert\Length(min="6", minMessage="Password should have at least 6 characters")
     * @Assert\Regex(
     *     pattern="/^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})/",
     *     message="Password should containt upper and lower case letters digits"
     * )
     */
    protected $password;

    /**
     * @ORM\Id
     * @ORM\Column(type="uuid")
     */
    protected $id;

    /**
     * @Assert\Regex(pattern="/^[\p{L}'][ \p{L}'-]*[\p{L}]$/u",message="First name should not contains anything except
     *     letters and spaces" )
     */
    private $firstName;

    /**
     * @Assert\Regex(pattern="/^[\p{L}'][ \p{L}'-]*[\p{L}]$/u",message="Last name should not contains anything except
     *     letters and spaces" )
     */
    private $lastName;

    private $gender;

    private $type;

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     *
     * @return User
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     *
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     *
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param mixed $gender
     *
     * @return User
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }
}
