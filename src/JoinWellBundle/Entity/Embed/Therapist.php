<?php

namespace JoinWellBundle\Entity\Embed;

/**
 * Class Therapist
 * @package JoinWellBundle\Entity\Embed
 */
class Therapist
{
    /**
     * Type of service that therapist provides
     *
     * @var string
     */
    private $type;

    /**
     * Rating of the therapist
     *
     * @var int
     */
    private $rating;

    /**
     * Language that therapist speaks
     *
     * @var string
     */
    private $language;

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     *
     * @return Therapist
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param mixed $rating
     *
     * @return Therapist
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param mixed $language
     *
     * @return Therapist
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }
}
