<?php

namespace JoinWellBundle\Entity\Embed;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class PatientPaymentInfo
 * @package JoinWellBundle\Entity\Embed
 */
class PatientPaymentInfo
{
    private $firstName;

    private $lastName;

    /**
     * @Assert\Valid
     */
    private $cryptoWallet;

    /**
     * @Assert\Valid
     */
    private $cardWallet;

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     *
     * @return PatientPaymentInfo
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     *
     * @return PatientPaymentInfo
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCryptoWallet()
    {
        return $this->cryptoWallet;
    }

    /**
     * @param mixed $cryptoWallet
     *
     * @return PatientPaymentInfo
     */
    public function setCryptoWallet($cryptoWallet)
    {
        $this->cryptoWallet = $cryptoWallet;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCardWallet()
    {
        return $this->cardWallet;
    }

    /**
     * @param mixed $cardWallet
     *
     * @return PatientPaymentInfo
     */
    public function setCardWallet($cardWallet)
    {
        $this->cardWallet = $cardWallet;

        return $this;
    }
}
