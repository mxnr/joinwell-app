<?php

namespace JoinWellBundle;


use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class JoinWellBundle
 * @package JoinWellBundle
 */
class JoinWellBundle extends Bundle
{
    /**
     * @return string
     */
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
