<?php

namespace JoinWellBundle\EventListener;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\Common\Persistence\ObjectManager;
use JoinWellBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

/**
 * Class LoginTracker
 * @package JoinWellBundle\EventListener
 */
class LoginTracker
{
    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    /**
     * @var ObjectManager|object
     */
    private $em;

    /**
     * Constructor
     *
     * @param TokenStorage $tokenStorage
     * @param Registry $registry
     */
    public function __construct(TokenStorage $tokenStorage, Registry $registry)
    {
        $this->tokenStorage = $tokenStorage;
        $this->em = $registry->getManager();
    }

    /**
     * Do the magic.
     *
     * @param InteractiveLoginEvent $event
     */
    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        if (($this->em instanceof ObjectManager) === false) {
            throw new \RuntimeException('registry access is not granted, unable to continue');
        }

        $currentToken = $this->tokenStorage->getToken();

        /** @var User $user */
        $user = $currentToken->getUser();
        //TODO: make discussion about updatedAt field, and it behavior in that case
        $user->setLastLogin(new \DateTime());
        $this->em->persist($user);
        $this->em->flush();
    }
}
