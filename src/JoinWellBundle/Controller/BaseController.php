<?php

namespace JoinWellBundle\Controller;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use JoinWellBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * Class BaseController
 *
 * @package JoinWellBundle\Controller
 */
class BaseController extends Controller
{
    /**
     * @return bool
     */
    public function isAuthenticated()
    {
        $anonymous = $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN');

        return $anonymous === false;
    }

    /**
     * Returns repository for entity
     *
     * @param string $entityName
     *
     * @return EntityRepository
     */
    protected function getRepository(string $entityName): EntityRepository
    {
        return $this->getManager()->getRepository($entityName);
    }

    /**
     * @return EntityManager
     */
    protected function getManager(): EntityManager
    {
        return $this->getDoctrine()->getManager();
    }
}
