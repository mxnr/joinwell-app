<?php

namespace JoinWellBundle\Controller;

use JoinWellBundle\Entity\Embed\Therapist as TherapistEmbed;
use JoinWellBundle\Entity\Therapist;
use JoinWellBundle\Form\Search\TherapistType;
use JoinWellBundle\Repository\TherapistRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CheckAvailabilityController
 *
 * @package JoinWellBundle\Controller
 */
class TherapistController extends BaseController
{
    /**
     * Process service availability for request
     *
     * @param Request $request
     *
     * @return Response
     */
    public function checkAvailabilityAction(Request $request): Response
    {
        /** @var TherapistRepository $repository */
        $repository = $this->getRepository(Therapist::class);
        $form = $this->createForm(TherapistType::class, new TherapistEmbed())->handleRequest($request);

        $list = null;
        $pages = 0;
        $found = 0;
        $perPage = 5;
        if ($form->isSubmitted() && $form->isValid()) {

            /** @var TherapistEmbed $entity */
            $entity = $form->getViewData();

            $list = $repository->setLimit($perPage)->checkAvailability(
                $entity->getType(),
                $entity->getLanguage(),
                $entity->getRating()
            );

            $pages = ceil($list->count() / $repository->getLimit());
            $found = $list->count();
        }

        return $this->render(
            'JoinWellBundle:Therapist:checkAvailability.html.twig',
            [
                'form' => $form->createView(),
                'list' => $list,
                'perPage' => $perPage,
                'pages' => $pages,
                'found' => $found,
            ]
        );
    }
}
