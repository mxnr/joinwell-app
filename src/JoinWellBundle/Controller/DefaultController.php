<?php

namespace JoinWellBundle\Controller;


use Symfony\Component\HttpFoundation\Response;


/**
 * Class DefaultController
 *
 * @package JoinWellBundle\Controller
 */
class DefaultController extends BaseController
{
    /**
     * @return Response
     */
    public function indexAction(): Response
    {
        return $this->render('JoinWellBundle:Default:index.html.twig');
    }
}
