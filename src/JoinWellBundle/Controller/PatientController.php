<?php

namespace JoinWellBundle\Controller;

use JoinWellBundle\Entity\Embed\InsuranceInfo;
use JoinWellBundle\Entity\Embed\PatientPaymentInfo;
use JoinWellBundle\Entity\Embed\PaymentInfo;
use JoinWellBundle\Entity\Episode;
use JoinWellBundle\Entity\Patient;
use JoinWellBundle\Entity\Therapist;
use JoinWellBundle\Entity\User;
use JoinWellBundle\Entity\Visit;
use JoinWellBundle\Entity\ZoomMeeting;
use JoinWellBundle\Form\Patient\InsuranceInfoType;
use JoinWellBundle\Form\Patient\PaymentInfoType;
use JoinWellBundle\Form\Patient\VisitType;
use JoinWellBundle\Repository\EpisodeRepository;
use JoinWellBundle\Repository\MeetingRepository;
use JoinWellBundle\Repository\PatientRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use ZoomBundle\Packet\Meeting\Create as ZoomRoom;
use ZoomBundle\Packet\User\CreateCust as ZoomUser;

/**
 * Class PatientController
 * @package JoinWellBundle\Controller
 */
class PatientController extends BaseController
{
    /**
     * @param Request $request
     *
     * @return Response
     */
    public function providePaymentInfoAction(Request $request): Response
    {
        /** @var User $myProfile */
        $myProfile = $this->getUser();

        $embedPaymentInfo = new PatientPaymentInfo();
        $form = $this->createForm(PaymentInfoType::class, $embedPaymentInfo)->handleRequest($request);

        $stored = false;
        if ($form->isSubmitted() === false) {
            /** @var PaymentInfo $formData */
            $formData = $form->getViewData();

            if ($formData->getFirstName() === '') {
                $formData
                    ->setFirstName($myProfile->getPerson()->getFirstName())
                    ->setLastName($myProfile->getPerson()->getLastName());
            }

            $form->setData($formData);
        } elseif ($form->isValid()) {
            $stored = true;
        }

        return $this->render(
            'JoinWellBundle:Patient:paymentInfo.html.twig',
            [
                'form' => $form->createView(),
                'stored' => $stored,
            ]
        );
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function provideInsuranceInfoAction(Request $request): Response
    {
        $embedInsuranceInfo = new InsuranceInfo();
        $form = $this->createForm(InsuranceInfoType::class, $embedInsuranceInfo)->handleRequest($request);

        $stored = false;
        if ($form->isSubmitted() && $form->isValid()) {
            $stored = true;
        }

        return $this->render(
            'JoinWellBundle:Patient:insuranceInfo.html.twig',
            [
                'form' => $form->createView(),
                'stored' => $stored,
            ]
        );
    }

    /**
     * @param Therapist $therapist
     * @param Request $request
     *
     * @return Response
     */
    public function createVisitAction(Therapist $therapist, Request $request): Response
    {
        $em = $this->getManager();

        $zoomService = $this->get('zoom.service');
        $zoomPacketManager = $zoomService->getManager();

        /** @var MeetingRepository $meetingRepository */
        $meetingRepository = $this->getRepository(ZoomMeeting::class);

        /** @var User $user */
        $user = $this->getUser();

        $meetingDurationMinutes = 30; //this is just for example purposes
        $timeZone = 'Europe/Kiev'; //this is just for example purposes, properly way is to take that param from user

        $patientName = $user->getPerson()->getFirstName().' '.$user->getPerson()->getLastName();
        $therapistName =
            $therapist->getUser()->getPerson()->getFirstName().
            ' '.
            $therapist->getUser()->getPerson()->getLastName();

        $visitEntity = new Visit();
        $visitEntity->setTherapist($therapist);

        /** @var PatientRepository $patientRepository */
        $patientRepository = $this->getRepository(Patient::class);
        /** @var EpisodeRepository $episodeRepository */
        $episodeRepository = $this->getRepository(Episode::class);
        $patient = $patientRepository->getByUser($this->getUser());
        $episodes = $episodeRepository->getActiveEpisodesForPatient($patient);

        $episodesChoices = ['--New--' => null];
        /** @var Episode $episode */
        foreach ($episodes as $episode) {
            $episodesChoices[$episode->getSubject()] = $episode;
        }

        $form = $this
            ->createForm(VisitType::class, $visitEntity, ['episodes' => $episodesChoices])
            ->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            //first of all export our therapist and patient to zoom
            {
                $therapistUser = $therapist->getUser();
                if ($therapistUser->hasZoomId() === false) {
                    $therapistZoomUser = new ZoomUser();
                    $therapistZoomUser
                        ->setEmail($therapistUser->getEmail())
                        ->setFirstName($therapistUser->getPerson()->getFirstName())
                        ->setLastName($therapistUser->getPerson()->getLastName())
                        ->setType(ZoomUser::TYPE_BASIC);
                    $response = $zoomPacketManager->flush($therapistZoomUser);
                    $therapistUser->setZoomId($response['id']);
                    $em->persist($therapistUser);
                    $em->flush();
                }

                if ($this->getUser()->hasZoomId() === false) {
                    $patientZoomUser = new ZoomUser();
                    $patientZoomUser
                        ->setEmail($user->getEmail())
                        ->setFirstName($user->getPerson()->getFirstName())
                        ->setLastName($user->getPerson()->getLastName())
                        ->setType(ZoomUser::TYPE_BASIC);
                    $response = $zoomPacketManager->flush($patientZoomUser);
                    $user->setZoomId($response['id']);
                    $em->persist($therapistUser);
                    $em->flush();
                }

                //then we will try to manage meeting for them
                $zoomMeetingRoom = new ZoomRoom();

                //TODO: take a discuss about China and India regions..
                $zoomMeetingRoom
                    ->setHostId($therapistUser->getZoomId())
                    ->setTopic(
                        sprintf(
                            'Visit %s to the doctor %s at %s',
                            $patientName,
                            $therapistName,
                            $visitEntity->getVisitAt()->format('Y-m-d H:i:s')
                        )
                    )
                    ->setType(ZoomRoom::TYPE_SCHEDULED)
                    ->setDuration($meetingDurationMinutes)
                    ->setStartTime($visitEntity->getVisitAt()->format('Y-m-d H:i:s'))
                    ->setTimeZone($timeZone)
                    ->setOptionRegistration(false)
                    ->setOptionStartType(ZoomRoom::START_TYPE_VIDEO)
                    ->setOptionHostVideo(true)
                    ->setOptionEnforceLogin(false)
                    ->setOptionAutoRecordType(ZoomRoom::RECORD_TYPE_NONE)
                    ->setOptionJbh(true); //allow patient to join into the meeting before therapist

                $zoomMeetingInfo = $zoomPacketManager->flush($zoomMeetingRoom);
                $zoomMeeting = new ZoomMeeting();
            }

            $episode = $visitEntity->getEpisode();
            if (empty($episode)) {
                $episodeEntity = new Episode();
                $episodeEntity
                    ->setPatient($patient)
                    ->setStatus(Episode::STATUS_NEW)
                    ->setSubject(
                        sprintf(
                            "Visit planned at: %s to the: %s",
                            (new \DateTime())->format('Y-m-d H:i:s'),
                            $therapistName
                        )
                    );
                $visitEntity->setEpisode($episodeEntity);
            }

            //and the last one
            $zoomMeeting
                ->setVisit($visitEntity)
                ->setHost($therapistUser)
                ->setHostUrl($zoomMeetingInfo['start_url'])
                ->setClient($user)
                ->setClientUrl($zoomMeetingInfo['join_url'])
                ->setZoomId($zoomMeetingInfo['id'])
                ->setScheduledAt($visitEntity->getVisitAt())
                ->setStatus(ZoomMeeting::STATUS_SCHEDULED);

            $em->persist($zoomMeeting);
            $em->flush();
        }

        /** @var ZoomMeeting[] $meetingsList */
        $meetingsList = $meetingRepository->getMeetingsGuestedBy($user);

        return $this->render(
            'JoinWellBundle:Patient:createVisit.html.twig',
            [
                'form' => $form->createView(),
                'therapist' => $therapist,
                'meetingsList' => $meetingsList,
            ]
        );
    }
}
