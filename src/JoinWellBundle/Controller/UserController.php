<?php

namespace JoinWellBundle\Controller;

use JoinWellBundle\Entity\Embed\User as UserEmbed;
use JoinWellBundle\Entity\Patient;
use JoinWellBundle\Entity\Person;
use JoinWellBundle\Entity\User;
use JoinWellBundle\Form\Register\UserType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * Class UserController
 * @package JoinWellBundle\Controller
 */
class UserController extends BaseController
{
    /**
     * @param Request $request
     *
     * @return Response
     */
    public function registerAction(Request $request): Response
    {
        $embedUserEntity = new UserEmbed();

        $form = $this->createForm(UserType::class, $embedUserEntity);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $personEntity = new Person();

            $personEntity
                ->setFirstName($embedUserEntity->getFirstName())
                ->setLastName($embedUserEntity->getLastName())
                ->setGender($embedUserEntity->getGender());


            $userEntity = new User();
            $userEntity
                ->setPerson($personEntity)
                ->setType($embedUserEntity->getType())
                ->setRoles(['ROLE_ADMIN'])
                ->setEnabled(true)
                ->setEmail($embedUserEntity->getEmail())
                ->setPlainPassword($embedUserEntity->getPassword());

            switch ($embedUserEntity->getType()) {
                case User::TYPE_PATIENT:
                    $rootTypeEntity = (new Patient())->setUser($userEntity);
                    break;
                default:
                    throw new \RuntimeException(
                        sprintf('unsupported type: <![CDATA[%s]]>', $embedUserEntity->getType())
                    );
            }

            $em = $this->getManager();
            $em->persist($rootTypeEntity);
            $em->flush();

            //authenticate that boy!
            $this->authenticate($userEntity);

            return $this->redirect($this->generateUrl('joinwell.home'));
        }

        return $this->render(
            'JoinWellBundle:User:signup.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    protected function authenticate(User $user): bool
    {
        if (empty($user->getId())) {
            return false;
        }

        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());

        $this->container->get('security.token_storage')->setToken($token);

        return true;
    }
}
