<?php

namespace ZoomBundle\Packet\Meeting;

use ZoomBundle\Contract\RequestPacket;

/**
 * Class GetInfo
 * @package ZoomBundle\Packet\Meeting
 */
class GetInfo implements RequestPacket
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $hostId;

    /**
     * @return string
     */
    public function getPath(): string
    {
        return 'meeting/get';
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return [
            'id' => $this->getId(),
            'host_id' => $this->getHostId(),
        ];
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function hasId(): bool
    {
        return !empty($this->id);
    }

    /**
     * @param string $id
     *
     * @return GetInfo
     */
    public function setId(string $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getHostId(): string
    {
        return $this->hostId;
    }

    /**
     * @return bool
     */
    public function hasHostId(): bool
    {
        return !empty($this->hostId);
    }

    /**
     * @param string $hostId
     *
     * @return GetInfo
     */
    public function setHostId(string $hostId)
    {
        $this->hostId = $hostId;

        return $this;
    }
}
