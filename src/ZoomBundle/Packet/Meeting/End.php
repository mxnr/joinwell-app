<?php

namespace ZoomBundle\Packet\Meeting;

/**
 * Class End
 * @package ZoomBundle\Packet\Meeting
 */
class End extends GetInfo
{
    /**
     * @return string
     */
    public function getPath(): string
    {
        return 'meeting/end';
    }
}
