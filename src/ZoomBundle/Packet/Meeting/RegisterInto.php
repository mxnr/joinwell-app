<?php

namespace ZoomBundle\Packet\Meeting;

use Symfony\Component\Intl\Intl;
use ZoomBundle\Contract\RequestPacket;

/**
 * Class RegisterInto
 * @package ZoomBundle\Packet\Meeting
 */
class RegisterInto implements RequestPacket
{
    const TIME_FRAME_WITHIN_MONTH = 'Within a month';

    const TIME_FRAME_1_TO_3_MONTHS = '1-3 months';

    const TIME_FRAME_4_TO_6_MONTHS = '4-6 months';

    const TIME_FRAME_MORE_THAN_6_MONTHS = 'More than 6 months';

    const TIME_FRAME_DISABLED = 'No timeframe';

    const PURCHASE_ROLE_PAYER = 'Decision Maker';

    const PURCHASE_ROLE_EVALUATOR = 'Evaluator/Recommender';

    const PURCHASE_ROLE_INFLUENCER = 'Influencer';

    const PURCHASE_ROLE_DISABLED = 'Not involved';

    const EMPLOYEES_COUNT_1_TO_20 = '1-20';

    const EMPLOYEES_COUNT_21_TO_50 = '21-50';

    const EMPLOYEES_COUNT_51_TO_100 = '51-100';

    const EMPLOYEES_COUNT_101_TO_250 = '101-250';

    const EMPLOYEES_COUNT_251_TO_500 = '251-500';

    const EMPLOYEES_COUNT_501_TO_1000 = '501-1,000';

    const EMPLOYEES_COUNT_1001_TO_5000 = '1,001-5,000';

    /**
     * id
     * required
     * Meeting ID.
     *
     * @var string
     */
    private $id;

    /**
     * email
     * required
     * A valid email address.
     *
     * @var string
     */
    private $email;

    /**
     * first_name
     * required
     * User’s first name.
     *
     * @var string
     */
    private $firstName;

    /**
     * last_name
     * required
     * User’s last name.
     *
     * @var string
     */
    private $lastName;

    /**
     * address
     * optional
     * Address.
     *
     * @var string
     */
    private $address;

    /**
     * city
     * optional
     * City.
     *
     * @var string
     */
    private $city;

    /**
     * country
     * optional
     * Country. For this parameter value please refer to the id value in country list
     *
     * @var string
     */
    private $country;

    /**
     * zip
     * optional
     * Zip/Postal Code.
     *
     * @var string
     */
    private $zip;

    /**
     * state
     * optional
     * State/Province.
     *
     * @var string
     */
    private $state;

    /**
     * phone
     * optional
     * Phone.
     *
     * @var string
     */
    private $phone;

    /**
     * industry
     * optional
     * Industry.
     *
     * @var string
     */
    private $industry;

    /**
     * org
     * optional
     * Organization.
     *
     * @var string
     */
    private $org;

    /**
     * job_title
     * optional
     * Job Title.
     */
    private $jobTitle;

    /**
     * purchasing_time_frame
     * optional
     * Purchasing Time Frame, should be “Within a month”, “1-3 months”, “4-6 months”, “More than 6 months”, “No
     * timeframe”.
     *
     * @var string
     */
    private $purchasingTimeFrame;

    /**
     * role_in_purchase_process
     * optional
     * Role in Purchase Process, should be “Decision Maker”, “Evaluator/Recommender”, “Influencer”, “Not involved”.
     *
     * @var string
     */
    private $roleInPurchaseProcess;

    /**
     * no_of_employees
     * optional
     * Number of Employees, should be “1-20”, “21-50”, “51-100”, “101-250”, “251-500”, “501-1,000”, “1,001-5,000”,
     * “5,001-10,000”, “More than 10,000”.
     *
     * @var string
     */
    private $noOfEmployees;

    /**
     * comments
     * optional
     * Questions & Comments.
     *
     * @var string
     */
    private $comments;

    /**
     * custom_questions
     * optional
     * Custom Questions, should be JSON format:
     * [{“title”:“field_name1”,“value”:“xxx”},{“title”:“field_name2”,“value”:“xxx”}].
     *
     * @var string
     */
    private $customQuestions;

    /**
     * language
     * optional
     * Language setting of email. This value should be “en-US”, “en”, “zh-CN”, “zh”, “en-ES”, “es”, “fr-FR” or “fr”.
     *
     * @var string
     */
    private $language;

    /**
     * occurrence_ids
     * optional
     * Occurrence IDs, could get this value from Meeting Get API.
     *
     * @var array
     */
    private $occurenceIds;

    /**
     * @return string
     */
    public function getPath(): string
    {
        return 'meeting/register';
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        $params = [
            'id' => $this->getId(),
            'email' => $this->getEmail(),
            'first_name' => $this->getFirstName(),
            'last_name' => $this->getLastName(),
        ];

        if ($this->hasAddress()) {
            $params['address'] = $this->getAddress();
        }

        if ($this->hasCity()) {
            $params['city'] = $this->getCity();
        }

        if ($this->getCountry()) {
            $params['country'] = $this->getCountry();
        }

        if ($this->hasZip()) {
            $params['zip'] = $this->getZip();
        }

        if ($this->hasState()) {
            $params['state'] = $this->getState();
        }

        if ($this->hasPhone()) {
            $params['phone'] = $this->getPhone();
        }

        if ($this->hasIndustry()) {
            $params['industry'] = $this->getIndustry();
        }

        if ($this->hasOrg()) {
            $params['org'] = $this->getOrg();
        }

        if ($this->hasJobTitle()) {
            $params['job_title'] = $this->getJobTitle();
        }

        if ($this->hasPurchasingTimeFrame()) {
            $params['purchasing_time_frame'] = $this->getPurchasingTimeFrame();
        }

        if ($this->hasRoleInPurchaseProcess()) {
            $params['role_in_purchase_process'] = $this->getRoleInPurchaseProcess();
        }

        if ($this->hasNoOfEmployees()) {
            $params['no_of_employees'] = $this->getNoOfEmployees();
        }

        if ($this->hasComments()) {
            $params['comments'] = $this->getComments();
        }

        if ($this->hasCustomQuestions()) {
            $params['custom_questions'] = $this->getCustomQuestions();
        }

        if ($this->hasLanguage()) {
            $params['language'] = $this->getLanguage();
        }

        if ($this->hasOccurenceIds()) {
            $params['occurrence_ids'] = $this->getOccurenceIds();
        }

        return $params;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function hasId(): bool
    {
        return !empty($this->id);
    }

    /**
     * @param string $id
     *
     * @return RegisterInto
     */
    public function setId(string $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return bool
     */
    public function hasEmail(): bool
    {
        return !empty($this->email);
    }

    /**
     * @param string $email
     *
     * @return RegisterInto
     */
    public function setEmail(string $email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return bool
     */
    public function hasFirstName(): bool
    {
        return !empty($this->firstName);
    }

    /**
     * @param string $firstName
     *
     * @return RegisterInto
     */
    public function setFirstName(string $firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @return bool
     */
    public function hasLastName(): bool
    {
        return !empty($this->lastName);
    }

    /**
     * @param string $lastName
     *
     * @return RegisterInto
     */
    public function setLastName(string $lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @return bool
     */
    public function hasAddress(): bool
    {
        return !empty($this->address);
    }

    /**
     * @param string $address
     *
     * @return RegisterInto
     */
    public function setAddress(string $address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @return bool
     */
    public function hasCity(): bool
    {
        return !empty($this->city);
    }

    /**
     * @param string $city
     *
     * @return RegisterInto
     */
    public function setCity(string $city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @return bool
     */
    public function hasCountry(): bool
    {
        return !empty($this->country);
    }

    /**
     * @param string $country
     *
     * @return RegisterInto
     */
    public function setCountry(string $country)
    {
        $countries = Intl::getRegionBundle()->getCountryNames();
        if (!isset($countries[$country])) {
            throw new \InvalidArgumentException('$country must be valid ISO2 country code');
        }

        $this->country = $country;

        return $this;
    }

    /**
     * @return string
     */
    public function getZip(): string
    {
        return $this->zip;
    }

    /**
     * @return bool
     */
    public function hasZip(): bool
    {
        return !empty($this->zip);
    }

    /**
     * @param string $zip
     *
     * @return RegisterInto
     */
    public function setZip(string $zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * @return string
     */
    public function getState(): string
    {
        return $this->state;
    }

    /**
     * @return bool
     */
    public function hasState(): bool
    {
        return !empty($this->state);
    }

    /**
     * @param string $state
     *
     * @return RegisterInto
     */
    public function setState(string $state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @return bool
     */
    public function hasPhone(): bool
    {
        return !empty($this->phone);
    }

    /**
     * @param string $phone
     *
     * @return RegisterInto
     */
    public function setPhone(string $phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return string
     */
    public function getIndustry(): string
    {
        return $this->industry;
    }

    /**
     * @return bool
     */
    public function hasIndustry(): bool
    {
        return !empty($this->industry);
    }

    /**
     * @param string $industry
     *
     * @return RegisterInto
     */
    public function setIndustry(string $industry)
    {
        $this->industry = $industry;

        return $this;
    }

    /**
     * @return string
     */
    public function getOrg(): string
    {
        return $this->org;
    }

    /**
     * @return bool
     */
    public function hasOrg(): bool
    {
        return !empty($this->org);
    }

    /**
     * @param string $org
     *
     * @return RegisterInto
     */
    public function setOrg(string $org)
    {
        $this->org = $org;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getJobTitle()
    {
        return $this->jobTitle;
    }

    /**
     * @return bool
     */
    public function hasJobTitle()
    {
        return !empty($this->jobTitle);
    }

    /**
     * @param mixed $jobTitle
     *
     * @return RegisterInto
     */
    public function setJobTitle($jobTitle)
    {
        $this->jobTitle = $jobTitle;

        return $this;
    }

    /**
     * @return string
     */
    public function getPurchasingTimeFrame(): string
    {
        return $this->purchasingTimeFrame;
    }

    /**
     * @return bool
     */
    public function hasPurchasingTimeFrame(): bool
    {
        return !empty($this->purchasingTimeFrame);
    }

    /**
     * @param string $purchasingTimeFrame
     *
     * @return RegisterInto
     */
    public function setPurchasingTimeFrame(string $purchasingTimeFrame)
    {
        $framesVariantList = [
            self::TIME_FRAME_1_TO_3_MONTHS,
            self::TIME_FRAME_4_TO_6_MONTHS,
            self::TIME_FRAME_MORE_THAN_6_MONTHS,
            self::TIME_FRAME_WITHIN_MONTH,
            self::TIME_FRAME_DISABLED,
        ];

        if (in_array($purchasingTimeFrame, $framesVariantList) == false) {
            throw new \InvalidArgumentException(
                sprintf(
                    '$purchasingTimeFrame must be one of [ %s ] values',
                    implode(' , ', $framesVariantList)
                )
            );
        }


        $this->purchasingTimeFrame = $purchasingTimeFrame;

        return $this;
    }

    /**
     * @return string
     */
    public function getRoleInPurchaseProcess(): string
    {
        return $this->roleInPurchaseProcess;
    }

    /**
     * @return bool
     */
    public function hasRoleInPurchaseProcess(): bool
    {
        return !empty($this->roleInPurchaseProcess);
    }

    /**
     * @param string $roleInPurchaseProcess
     *
     * @return RegisterInto
     */
    public function setRoleInPurchaseProcess(string $roleInPurchaseProcess)
    {
        $purchaseRoleVarianList = [
            self::PURCHASE_ROLE_DISABLED,
            self::PURCHASE_ROLE_EVALUATOR,
            self::PURCHASE_ROLE_INFLUENCER,
            self::PURCHASE_ROLE_PAYER,
        ];

        if (in_array($roleInPurchaseProcess, $purchaseRoleVarianList) === false) {
            throw new \InvalidArgumentException(
                sprintf(
                    '$roleInPurchaseProcess must be one of [ %s ] values',
                    implode(', ', $purchaseRoleVarianList)
                )
            );
        }

        $this->roleInPurchaseProcess = $roleInPurchaseProcess;

        return $this;
    }

    /**
     * @return string
     */
    public function getNoOfEmployees(): string
    {
        return $this->noOfEmployees;
    }

    /**
     * @return bool
     */
    public function hasNoOfEmployees(): bool
    {
        return !empty($this->noOfEmployees);
    }

    /**
     * @param string $noOfEmployees
     *
     * @return RegisterInto
     */
    public function setNoOfEmployees(string $noOfEmployees)
    {
        $employeesVariantList = [
            self::EMPLOYEES_COUNT_1_TO_20,
            self::EMPLOYEES_COUNT_21_TO_50,
            self::EMPLOYEES_COUNT_51_TO_100,
            self::EMPLOYEES_COUNT_101_TO_250,
            self::EMPLOYEES_COUNT_251_TO_500,
            self::EMPLOYEES_COUNT_501_TO_1000,
            self::EMPLOYEES_COUNT_1001_TO_5000,
        ];
        if (in_array($noOfEmployees, $employeesVariantList) === false) {
            throw new \InvalidArgumentException(
                sprintf(
                    '$noOfEmployees must be one of [ %s ] values',
                    implode(', ', $employeesVariantList)
                )
            );
        }

        $this->noOfEmployees = $noOfEmployees;

        return $this;
    }

    /**
     * @return string
     */
    public function getComments(): string
    {
        return $this->comments;
    }

    /**
     * @return bool
     */
    public function hasComments(): bool
    {
        return !empty($this->comments);
    }

    /**
     * @param string $comments
     *
     * @return RegisterInto
     */
    public function setComments(string $comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * @return string
     */
    public function getCustomQuestions(): string
    {
        return $this->customQuestions;
    }

    /**
     * @return bool
     */
    public function hasCustomQuestions(): bool
    {
        return !empty($this->customQuestions);
    }

    /**
     * @param string $customQuestions
     *
     * @return RegisterInto
     */
    public function setCustomQuestions(string $customQuestions)
    {
        $this->customQuestions = $customQuestions;

        return $this;
    }

    /**
     * @return string
     */
    public function getLanguage(): string
    {
        return $this->language;
    }

    /**
     * @return bool
     */
    public function hasLanguage(): bool
    {
        return !empty($this->language);
    }

    /**
     * @param string $language
     *
     * @return RegisterInto
     */
    public function setLanguage(string $language)
    {
        $languageList = ['en-US', 'en', 'zh-CN', 'zh', 'en-ES', 'es', 'fr-FR', 'fr'];

        if (in_array($language, $languageList) === false) {
            throw new \InvalidArgumentException(
                sprintf(
                    '$language must be one of [ %s ] values',
                    implode(', ', $languageList)
                )
            );
        }

        $this->language = $language;

        return $this;
    }

    /**
     * @return array
     */
    public function getOccurenceIds(): array
    {
        return $this->occurenceIds;
    }

    /**
     * @return bool
     */
    public function hasOccurenceIds(): bool
    {
        return !empty($this->occurenceIds);
    }

    /**
     * @param array $occurenceIds
     *
     * @return RegisterInto
     */
    public function setOccurenceIds(array $occurenceIds)
    {
        $this->occurenceIds = $occurenceIds;

        return $this;
    }
}
