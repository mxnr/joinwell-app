<?php

namespace ZoomBundle\Packet\Meeting;

use DateTimeZone;
use ZoomBundle\Contract\RequestPacket;

/**
 * Class Create
 * @package ZoomBundle\Packet\Meeting
 */
class Create implements RequestPacket
{
    const TYPE_INSTANT = 1;

    const TYPE_SCHEDULED = 2;

    const TYPE_RECURRING = 3;

    const TYPE_SCHEDULED_RECURRING = 2;

    const REGISTRATION_TYPE_CAN_ATTEND_ANY = 1;

    const REGISTRATION_TYPE_REGISTER_BEFORE_ATTEND = 2;

    const REGISTRATION_TYPE_CAN_ATTEND_ANY_AFTER_REGISTER = 3;

    const START_TYPE_VIDEO = 'video';

    const START_TYPE_SCREENSHARE = 'screen_share';

    const AUDIO_TELEPHONY = 'telephony';

    const AUDIO_BOTH = 'both';

    const AUDIO_VOIP = 'voip';

    const RECORD_TYPE_LOCAL = 'local';

    const RECORD_TYPE_CLOUD = 'cloud';

    const RECORD_TYPE_NONE = 'none';

    /**
     * host_id
     * required
     * Meeting host user ID. Can be any user under this account. Cannot be updated after creation.
     *
     * @var string
     */
    private $hostId;

    /**
     * type
     * required
     * Meeting type: 1 means instant meeting (Only used for host to start it as soon as created). 2 means normal
     * scheduled meeting. 3 means a recurring meeting with no fixed time. 8 means a recurring meeting with fixed time.
     * Default: 2
     *
     * @var int
     */
    private $type = self::TYPE_SCHEDULED;

    /**
     * topic
     * required
     * Meeting topic. Max of 300 characters.
     *
     * @var string
     */
    private $topic;

    /**
     * start_time
     * optional
     * Meeting start time in ISO datetime format. For scheduled meeting and recurring meeting with fixed time. Should
     * be UTC time, such as 2012-11-25T12:00:00Z.
     *
     * @var string
     */
    private $startTime;

    /**
     * duration
     * optional
     * Meeting duration (minutes). For scheduled meeting only.
     *
     * @var int
     */
    private $duration;

    /**
     * timezone
     * optional
     * Timezone to format start_time, like “America/Los_Angeles”. For scheduled meeting only. For this parameter value
     * please refer to the id value in timezone list.
     *
     * @var string
     */
    private $timeZone;

    /**
     * password
     * optional
     * Meeting password. Password may only contain the following characters: [a-z A-Z 0-9 @ - _ *]. Max of 10
     * characters.
     *
     * @var string
     */
    private $password;

    /**
     * recurrence
     * optional
     * Recurrence Meeting Settings. For recurring meeting with fixed time only. See Recurrence Object for more details.
     *
     * @var array
     */
    private $recurrence;

    /**
     * option_registration
     * optional
     * Registration required.
     * Default: false
     *
     * @var bool
     */
    private $optionRegistration = false;

    /**
     * registration_type
     * optional
     * Registration type. 1 means Attendees register once and can attend any of the occurrences, 2 means Attendees need
     * to register for each occurrence to attend, 3 means Attendees register once and can choose one or more
     * occurrences to attend. For recurring meeting with fixed time only. Default: 1
     *
     * @var int
     */
    private $registrationType = 1;

    /**
     * option_jbh
     * optional
     * Join meeting before host start the meeting. Only for scheduled or recurring meetings.
     * Default: false
     *
     * @var bool
     */
    private $optionJbh = false;

    /**
     * option_start_type
     * optional
     * Meeting start type. Can be “video” or “screen_share”. (deprecated)
     * Default: video
     *
     * @var string
     */
    private $optionStartType = 'video';

    /**
     * option_host_video
     * optional
     * Start video when host join meeting.
     * Default: true
     *
     * @var bool
     */
    private $optionHostVideo = true;

    /**
     * option_participants_video
     * optional
     * Start video when participants join meeting.
     * Default: true
     *
     * @var bool
     */
    private $optionParticipantsVideo = true;

    /**
     * option_cn_meeting
     * optional
     * Host meeting in China.
     * Default: false
     *
     * @var bool
     */
    private $optionCnMeeting = false;

    /**
     * option_in_meeting
     * optional
     * Host meeting in India.
     * Default: false
     *
     * @var bool
     */
    private $optionInMeeting = false;

    /**
     * option_audio
     * optional
     * Meeting audio options. Can be “both”, “telephony”, “voip”.
     * Default: both
     *
     * @var string
     */
    private $optionAudio = 'both';

    /**
     * option_enforce_login
     * optional
     * Only signed-in users can join this meeting.
     * Default: false
     *
     * @var bool
     */
    private $optionEnforceLogin = false;

    /**
     * option_enforce_login_domains
     * optional
     * Only signed-in users with specified domains can join meetings.
     *
     * @var array
     */
    private $optionEnforceLoginDomains;

    /**
     * option_alternative_hosts
     * optional
     * Alternative hosts.
     *
     * @var array
     */
    private $optionAlternativeHosts;

    /**
     * option_alternative_host_ids
     * optional
     * Alternative hosts IDs.
     *
     * @var array
     */
    private $optionAlternativeHostIds;

    /**
     * option_use_pmi
     * optional
     * Use Personal Meeting ID. Only for scheduled meetings.
     * Default: false
     * @var bool
     */
    private $optionUsePmi = false;

    /**
     * option_auto_record_type
     * optional
     * Automatically record type. Can be “local”, “cloud” or “none”.
     * Default: local
     *
     * @var string
     */
    private $optionAutoRecordType = 'local';

    /**
     * @return string
     */
    public function getPath(): string
    {
        return 'meeting/create';
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        $params = [
            'host_id' => $this->getHostId(),
            'type' => $this->getType(),
            'topic' => $this->getTopic(),
        ];

        if ($this->hasStartTime()) {
            $params['start_time'] = $this->getStartTime();
        }

        if ($this->hasDuration()) {
            $params['duration'] = $this->getDuration();
        }

        if ($this->hasTimeZone()) {
            $params['timezone'] = $this->getTimeZone();
        }

        if ($this->hasPassword()) {
            $params['password'] = $this->getPassword();
        }

        if ($this->hasRecurrence()) {
            $params['recurrence'] = $this->getRecurrence();
        }

        if ($this->hasOptionRegistration()) {
            $params['option_registration'] = $this->isOptionRegistration();
        }

        if ($this->hasRegistrationType()) {
            $params['registration_type'] = $this->getRegistrationType();
        }

        if ($this->hasOptionJbh()) {
            $params['option_jbh'] = $this->isOptionJbh();
        }

        if ($this->hasOptionStartType()) {
            $params['option_start_type'] = $this->getOptionStartType();
        }

        if ($this->hasOptionHostVideo()) {
            $params['option_host_video'] = $this->isOptionHostVideo();
        }

        if ($this->hasOptionParticipantsVideo()) {
            $params['option_participants_video'] = $this->isOptionParticipantsVideo();
        }

        if ($this->hasOptionCnMeeting()) {
            $params['option_cn_meeting'] = $this->isOptionCnMeeting();
        }

        if ($this->hasOptionInMeeting()) {
            $params['option_in_meeting'] = $this->isOptionInMeeting();
        }

        if ($this->hasOptionAudio()) {
            $params['option_audio'] = $this->getOptionAudio();
        }

        if ($this->hasOptionEnforceLogin()) {
            $params['option_enforce_login'] = $this->isOptionEnforceLogin();
        }

        if ($this->hasOptionEnforceLoginDomains()) {
            $params['option_enforce_login_domains'] = $this->getOptionEnforceLoginDomains();
        }

        if ($this->hasOptionAlternativeHosts()) {
            $params['option_alternative_hosts'] = $this->getOptionAlternativeHosts();
        }

        if ($this->hasOptionAlternativeHostIds()) {
            $params['option_alternative_host_ids'] = $this->getOptionAlternativeHostIds();
        }

        if ($this->hasOptionUsePmi()) {
            $params['option_use_pmi'] = $this->isOptionUsePmi();
        }

        if ($this->hasOptionAutoRecordType()) {
            $params['option_auto_record_type'] = $this->getOptionAutoRecordType();
        }

        return $params;
    }

    /**
     * @return mixed
     */
    public function getHostId()
    {
        return $this->hostId;
    }

    /**
     * @return bool
     */
    public function hasHostId()
    {
        return !empty($this->hostId);
    }

    /**
     * @param mixed $hostId
     *
     * @return Create
     */
    public function setHostId($hostId)
    {
        $this->hostId = $hostId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return bool
     */
    public function hasType()
    {
        return !empty($this->type);
    }

    /**
     * @param mixed $type
     *
     * @return Create
     */
    public function setType($type)
    {
        if (in_array(
                $type,
                [self::TYPE_INSTANT, self::TYPE_SCHEDULED, self::TYPE_RECURRING, self::TYPE_SCHEDULED_RECURRING]
            ) === false
        ) {
            throw new \InvalidArgumentException(
                sprintf(
                    '$type must be on of [ %s ] values',
                    implode(
                        ', ',
                        [self::TYPE_INSTANT, self::TYPE_SCHEDULED, self::TYPE_RECURRING, self::TYPE_SCHEDULED_RECURRING]
                    )
                )
            );
        }

        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getTopic(): string
    {
        return $this->topic;
    }

    /**
     * @return bool
     */
    public function hasTopic(): bool
    {
        return !empty($this->topic);
    }

    /**
     * @param string $topic
     *
     * @return Create
     */
    public function setTopic(string $topic)
    {
        $this->topic = $topic;

        return $this;
    }

    /**
     * @return string
     */
    public function getStartTime(): string
    {
        return $this->startTime;
    }

    /**
     * @return bool
     */
    public function hasStartTime(): bool
    {
        return !empty($this->startTime);
    }

    /**
     * @param string $startTime
     *
     * @return Create
     */
    public function setStartTime(string $startTime)
    {
        $this->startTime = $startTime;

        return $this;
    }

    /**
     * @return int
     */
    public function getDuration(): int
    {
        return $this->duration;
    }

    /**
     * @return bool
     */
    public function hasDuration(): bool
    {
        return !empty($this->duration);
    }

    /**
     * @param int $duration
     *
     * @return Create
     */
    public function setDuration(int $duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * @return string
     */
    public function getTimeZone(): string
    {
        return $this->timeZone;
    }

    /**
     * @return bool
     */
    public function hasTimeZone(): bool
    {
        return !empty($this->timeZone);
    }

    /**
     * @param string $timeZone
     *
     * @return Create
     */
    public function setTimeZone(string $timeZone)
    {
        if (in_array($timeZone, DateTimeZone::listIdentifiers()) === false) {
            throw new \InvalidArgumentException('$timeZone must be valid timezone name');
        }

        $this->timeZone = $timeZone;

        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @return bool
     */
    public function hasPassword(): bool
    {
        return !empty($this->password);
    }

    /**
     * @param string $password
     *
     * @return Create
     */
    public function setPassword(string $password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return array
     */
    public function getRecurrence(): array
    {
        return $this->recurrence;
    }

    /**
     * @return bool
     */
    public function hasRecurrence(): bool
    {
        return !empty($this->recurrence);
    }

    /**
     * @param array $recurrence
     *
     * @return Create
     */
    public function setRecurrence(array $recurrence)
    {
        $this->recurrence = $recurrence;

        return $this;
    }

    /**
     * @return bool
     */
    public function isOptionRegistration(): bool
    {
        return $this->optionRegistration;
    }

    /**
     * @return bool
     */
    public function hasOptionRegistration(): bool
    {
        return !empty($this->optionRegistration);
    }

    /**
     * @param bool $optionRegistration
     *
     * @return Create
     */
    public function setOptionRegistration(bool $optionRegistration)
    {
        $this->optionRegistration = $optionRegistration;

        return $this;
    }

    /**
     * @return int
     */
    public function getRegistrationType(): int
    {
        return $this->registrationType;
    }

    /**
     * @return bool
     */
    public function hasRegistrationType(): bool
    {
        return !empty($this->registrationType);
    }

    /**
     * @param int $registrationType
     *
     * @return Create
     */
    public function setRegistrationType(int $registrationType)
    {
        if (in_array(
                $registrationType,
                [
                    self::REGISTRATION_TYPE_CAN_ATTEND_ANY,
                    self::REGISTRATION_TYPE_REGISTER_BEFORE_ATTEND,
                    self::REGISTRATION_TYPE_CAN_ATTEND_ANY_AFTER_REGISTER,
                ]
            ) === false
        ) {
            throw new \InvalidArgumentException(
                sprintf(
                    '$registrationType must be on of [ %s ] values',
                    implode(
                        ', ',
                        [
                            self::REGISTRATION_TYPE_CAN_ATTEND_ANY,
                            self::REGISTRATION_TYPE_REGISTER_BEFORE_ATTEND,
                            self::REGISTRATION_TYPE_CAN_ATTEND_ANY_AFTER_REGISTER,
                        ]
                    )
                )
            );
        }

        $this->registrationType = $registrationType;

        return $this;
    }

    /**
     * @return bool
     */
    public function isOptionJbh(): bool
    {
        return $this->optionJbh;
    }

    /**
     * @return bool
     */
    public function hasOptionJbh(): bool
    {
        return !empty($this->optionJbh);
    }

    /**
     * @param bool $optionJbh
     *
     * @return Create
     */
    public function setOptionJbh(bool $optionJbh)
    {
        $this->optionJbh = $optionJbh;

        return $this;
    }

    /**
     * @return string
     */
    public function getOptionStartType(): string
    {
        return $this->optionStartType;
    }

    /**
     * @return bool
     */
    public function hasOptionStartType(): bool
    {
        return !empty($this->optionStartType);
    }

    /**
     * @param string $optionStartType
     *
     * @return Create
     */
    public function setOptionStartType(string $optionStartType)
    {
        if (in_array($optionStartType, [self::START_TYPE_SCREENSHARE, self::START_TYPE_VIDEO]) === false) {
            throw new \InvalidArgumentException(
                '$optionStartType must be one of [ %s ] values',
                implode(', ', [self::START_TYPE_SCREENSHARE, self::START_TYPE_VIDEO])
            );
        }

        $this->optionStartType = $optionStartType;

        return $this;
    }

    /**
     * @return bool
     */
    public function isOptionHostVideo(): bool
    {
        return $this->optionHostVideo;
    }

    /**
     * @return bool
     */
    public function hasOptionHostVideo(): bool
    {
        return !empty($this->optionHostVideo);
    }

    /**
     * @param bool $optionHostVideo
     *
     * @return Create
     */
    public function setOptionHostVideo(bool $optionHostVideo)
    {
        $this->optionHostVideo = $optionHostVideo;

        return $this;
    }

    /**
     * @return bool
     */
    public function isOptionParticipantsVideo(): bool
    {
        return $this->optionParticipantsVideo;
    }

    /**
     * @return bool
     */
    public function hasOptionParticipantsVideo(): bool
    {
        return !empty($this->optionParticipantsVideo);
    }

    /**
     * @param bool $optionParticipantsVideo
     *
     * @return Create
     */
    public function setOptionParticipantsVideo(bool $optionParticipantsVideo)
    {
        $this->optionParticipantsVideo = $optionParticipantsVideo;

        return $this;
    }

    /**
     * @return bool
     */
    public function isOptionCnMeeting(): bool
    {
        return $this->optionCnMeeting;
    }

    /**
     * @return bool
     */
    public function hasOptionCnMeeting(): bool
    {
        return !empty($this->optionCnMeeting);
    }

    /**
     * @param bool $optionCnMeeting
     *
     * @return Create
     */
    public function setOptionCnMeeting(bool $optionCnMeeting)
    {
        $this->optionCnMeeting = $optionCnMeeting;

        return $this;
    }

    /**
     * @return bool
     */
    public function isOptionInMeeting(): bool
    {
        return $this->optionInMeeting;
    }

    /**
     * @return bool
     */
    public function hasOptionInMeeting(): bool
    {
        return !empty($this->optionInMeeting);
    }

    /**
     * @param bool $optionInMeeting
     *
     * @return Create
     */
    public function setOptionInMeeting(bool $optionInMeeting)
    {
        $this->optionInMeeting = $optionInMeeting;

        return $this;
    }

    /**
     * @return string
     */
    public function getOptionAudio(): string
    {
        return $this->optionAudio;
    }

    /**
     * @return bool
     */
    public function hasOptionAudio(): bool
    {
        return !empty($this->optionAudio);
    }

    /**
     * @param string $optionAudio
     *
     * @return Create
     */
    public function setOptionAudio(string $optionAudio)
    {
        if (in_array($optionAudio, [self::AUDIO_BOTH, self::AUDIO_TELEPHONY, self::AUDIO_VOIP]) === false) {
            throw new \InvalidArgumentException(
                sprintf(
                    '$optionAudio must be one of [ %s ] values',
                    implode(', ', [self::AUDIO_BOTH, self::AUDIO_TELEPHONY, self::AUDIO_VOIP])
                )
            );
        }

        $this->optionAudio = $optionAudio;

        return $this;
    }

    /**
     * @return bool
     */
    public function isOptionEnforceLogin(): bool
    {
        return $this->optionEnforceLogin;
    }

    /**
     * @return bool
     */
    public function hasOptionEnforceLogin(): bool
    {
        return !empty($this->optionEnforceLogin);
    }

    /**
     * @param bool $optionEnforceLogin
     *
     * @return Create
     */
    public function setOptionEnforceLogin(bool $optionEnforceLogin)
    {
        $this->optionEnforceLogin = $optionEnforceLogin;

        return $this;
    }

    /**
     * @return array
     */
    public function getOptionEnforceLoginDomains(): array
    {
        return $this->optionEnforceLoginDomains;
    }

    /**
     * @return bool
     */
    public function hasOptionEnforceLoginDomains(): bool
    {
        return !empty($this->optionEnforceLoginDomains);
    }

    /**
     * @param array $optionEnforceLoginDomains
     *
     * @return Create
     */
    public function setOptionEnforceLoginDomains(array $optionEnforceLoginDomains)
    {
        $this->optionEnforceLoginDomains = $optionEnforceLoginDomains;

        return $this;
    }

    /**
     * @return array
     */
    public function getOptionAlternativeHosts(): array
    {
        return $this->optionAlternativeHosts;
    }

    /**
     * @return bool
     */
    public function hasOptionAlternativeHosts(): bool
    {
        return !empty($this->optionAlternativeHosts);
    }

    /**
     * @param array $optionAlternativeHosts
     *
     * @return Create
     */
    public function setOptionAlternativeHosts(array $optionAlternativeHosts)
    {
        $this->optionAlternativeHosts = $optionAlternativeHosts;

        return $this;
    }

    /**
     * @return array
     */
    public function getOptionAlternativeHostIds(): array
    {
        return $this->optionAlternativeHostIds;
    }

    /**
     * @return bool
     */
    public function hasOptionAlternativeHostIds(): bool
    {
        return !empty($this->optionAlternativeHostIds);
    }

    /**
     * @param array $optionAlternativeHostIds
     *
     * @return Create
     */
    public function setOptionAlternativeHostIds(array $optionAlternativeHostIds)
    {
        $this->optionAlternativeHostIds = $optionAlternativeHostIds;

        return $this;
    }

    /**
     * @return bool
     */
    public function isOptionUsePmi(): bool
    {
        return $this->optionUsePmi;
    }

    /**
     * @return bool
     */
    public function hasOptionUsePmi(): bool
    {
        return !empty($this->optionUsePmi);
    }

    /**
     * @param bool $optionUsePmi
     *
     * @return Create
     */
    public function setOptionUsePmi(bool $optionUsePmi)
    {
        $this->optionUsePmi = $optionUsePmi;

        return $this;
    }

    /**
     * @return string
     */
    public function getOptionAutoRecordType(): string
    {
        return $this->optionAutoRecordType;
    }

    /**
     * @return bool
     */
    public function hasOptionAutoRecordType(): bool
    {
        return !empty($this->optionAutoRecordType);
    }

    /**
     * @param string $optionAutoRecordType
     *
     * @return Create
     */
    public function setOptionAutoRecordType(string $optionAutoRecordType)
    {
        if (in_array(
                $optionAutoRecordType,
                [self::RECORD_TYPE_LOCAL, self::RECORD_TYPE_CLOUD, self::RECORD_TYPE_NONE]
            ) === false
        ) {
            throw new \InvalidArgumentException(
                sprintf(
                    '$optionAutoRecordType must be one of [ %s ] values',
                    implode(', ', [self::RECORD_TYPE_LOCAL, self::RECORD_TYPE_CLOUD, self::RECORD_TYPE_NONE])
                )
            );
        }

        $this->optionAutoRecordType = $optionAutoRecordType;

        return $this;
    }
}
