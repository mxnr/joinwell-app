<?php

namespace ZoomBundle\Packet\Meeting;

use ZoomBundle\Contract\RequestPacket;

/**
 * Class GetList
 * @package ZoomBundle\Packet\Meeting
 */
class GetList implements RequestPacket
{
    /**
     * @var string
     */
    private $hostId;

    /**
     * @var int
     */
    private $pageSize = 20;

    /**
     * @var int
     */
    private $pageNumber = 1;

    /**
     * @return string
     */
    public function getPath(): string
    {
        return 'meeting/list';
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return [
            'host_id' => $this->getHostId(),
            'page_size' => $this->getPageSize(),
            'page_number' => $this->getPageNumber(),
        ];
    }

    /**
     * @return string
     */
    public function getHostId(): string
    {
        return $this->hostId;
    }

    /**
     * @return bool
     */
    public function hasHostId(): bool
    {
        return !empty($this->hostId);
    }

    /**
     * @param string $hostId
     *
     * @return GetList
     */
    public function setHostId(string $hostId)
    {
        $this->hostId = $hostId;

        return $this;
    }

    /**
     * @return int
     */
    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * @return bool
     */
    public function hasPageSize(): bool
    {
        return !empty($this->pageSize);
    }

    /**
     * @param int $pageSize
     *
     * @return GetList
     */
    public function setPageSize(int $pageSize)
    {
        $this->pageSize = $pageSize;

        return $this;
    }

    /**
     * @return int
     */
    public function getPageNumber(): int
    {
        return $this->pageNumber;
    }

    /**
     * @return bool
     */
    public function hasPageNumber(): bool
    {
        return !empty($this->pageNumber);
    }

    /**
     * @param int $pageNumber
     *
     * @return GetList
     */
    public function setPageNumber(int $pageNumber)
    {
        $this->pageNumber = $pageNumber;

        return $this;
    }
}
