<?php

namespace ZoomBundle\Packet\Meeting;

/**
 * Class Delete
 * @package ZoomBundle\Packet\Meeting
 */
class Delete extends GetInfo
{
    private $occurenceId;

    /**
     * @return string
     */
    public function getPath(): string
    {
        return 'meeting/delete';
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        $params = parent::getParams();

        if ($this->hasOccurenceId()) {
            $params['occurrence_id'] = $this->getOccurenceId();
        }

        return $params;
    }

    /**
     * @return mixed
     */
    public function getOccurenceId()
    {
        return $this->occurenceId;
    }

    /**
     * @return bool
     */
    public function hasOccurenceId()
    {
        return !empty($this->occurenceId);
    }

    /**
     * @param mixed $occurenceId
     *
     * @return Delete
     */
    public function setOccurenceId($occurenceId)
    {
        $this->occurenceId = $occurenceId;

        return $this;
    }
}
