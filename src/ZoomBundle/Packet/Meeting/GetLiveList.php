<?php

namespace ZoomBundle\Packet\Meeting;

use ZoomBundle\Contract\RequestPacket;

/**
 * Class GetLiveList
 * @package ZoomBundle\Packet\Meeting
 */
class GetLiveList implements RequestPacket
{
    private $pageNumber = 1;

    private $pageSize = 20;

    /**
     * @return string
     */
    public function getPath(): string
    {
        return 'meeting/live';
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return [
            'page_size' => $this->getPageSize(),
            'page_number' => $this->getPageNumber(),
        ];
    }

    /**
     * @return int
     */
    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * @return bool
     */
    public function hasPageSize(): bool
    {
        return !empty($this->pageSize);
    }

    /**
     * @param int $pageSize
     *
     * @return GetLiveList
     */
    public function setPageSize(int $pageSize)
    {
        $this->pageSize = $pageSize;

        return $this;
    }

    /**
     * @return int
     */
    public function getPageNumber(): int
    {
        return $this->pageNumber;
    }

    /**
     * @return bool
     */
    public function hasPageNumber(): bool
    {
        return !empty($this->pageNumber);
    }

    /**
     * @param int $pageNumber
     *
     * @return GetLiveList
     */
    public function setPageNumber(int $pageNumber)
    {
        $this->pageNumber = $pageNumber;

        return $this;
    }
}
