<?php

namespace ZoomBundle\Packet\User;

/**
 * Class Deactivate
 * @package ZoomBundle\Packet\User
 */
class Deactivate extends Activate
{
    /**
     * @return string
     */
    public function getPath(): string
    {
        return 'user/deactivate';
    }
}
