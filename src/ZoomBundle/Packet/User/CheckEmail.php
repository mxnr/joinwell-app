<?php

namespace ZoomBundle\Packet\User;

use ZoomBundle\Contract\RequestPacket;

/**
 * Class CheckEmail
 * @package ZoomBundle\Packet\User
 */
class CheckEmail implements RequestPacket
{
    /**
     * @var string
     */
    private $email;

    /**
     * @return string
     */
    public function getPath(): string
    {
        return 'user/checkemail';
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return [
            'email' => $this->getEmail(),
        ];
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return bool
     */
    public function hasEmail(): bool
    {
        return !empty($this->email);
    }

    /**
     * @param string $email
     *
     * @return CheckEmail
     */
    public function setEmail(string $email)
    {
        $this->email = $email;

        return $this;
    }
}
