<?php

namespace ZoomBundle\Packet\User;

use ZoomBundle\Contract\RequestPacket;

/**
 * Class Delete
 * @package ZoomBundle\Packet\User
 */
class Delete implements RequestPacket
{
    /**
     *
     */
    const TYPE_EXISTING = 0;

    /**
     *
     */
    const TYPE_PENDING = 1;

    /**
     * @var string
     */
    private $id;

    /**
     * @var int
     */
    private $type = 0;

    /**
     * @return string
     */
    public function getPath(): string
    {
        return 'user/delete';
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return [
            'id' => $this->getId(),
            'type' => $this->getType(),
        ];
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function hasId(): bool
    {
        return !empty($this->id);
    }

    /**
     * @param string $id
     *
     * @return Delete
     */
    public function setId(string $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @return bool
     */
    public function hasType(): bool
    {
        return !empty($this->type);
    }

    /**
     * @param int $type
     *
     * @return Delete
     */
    public function setType(int $type)
    {
        if (in_array($type, [self::TYPE_EXISTING, self::TYPE_PENDING]) === false) {
            throw new \InvalidArgumentException(
                sprintf('$type must be one of [ %s ] values', [self::TYPE_EXISTING, self::TYPE_PENDING])
            );
        }

        $this->type = $type;

        return $this;
    }
}
