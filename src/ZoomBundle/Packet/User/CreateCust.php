<?php

namespace ZoomBundle\Packet\User;

/**
 * Class CreateCust
 * @package ZoomBundle\Packet\User
 */
class CreateCust extends Create
{
    /**
     * @return string
     */
    public function getPath(): string
    {
        return 'user/custcreate';
    }
}
