<?php

namespace ZoomBundle\Packet\User;

/**
 * Class UpdateInfo
 * @package ZoomBundle\Packet\User
 */
class UpdateInfo extends Create
{
    /**
     * @var string
     */
    private $id;

    /**
     * @return string
     */
    public function getPath(): string
    {
        return 'user/update';
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        $params = parent::getParams();
        $params['id'] = $this->getId();

        return $params;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function hasId(): bool
    {
        return !empty($this->id);
    }

    /**
     * @param string $id
     *
     * @return UpdateInfo
     */
    public function setId(string $id)
    {
        $this->id = $id;

        return $this;
    }
}
