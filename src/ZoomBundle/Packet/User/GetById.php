<?php

namespace ZoomBundle\Packet\User;

use ZoomBundle\Contract\RequestPacket;

/**
 * Class GetById
 * @package ZoomBundle\Entity\User
 */
class GetById implements RequestPacket
{
    /**
     * @var string
     */
    private $id;

    /**
     * @return string
     */
    public function getPath(): string
    {
        return 'user/get';
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return [
            'id' => $this->getId(),
        ];
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return GetById
     */
    public function setId(string $id)
    {
        $this->id = $id;

        return $this;
    }
}
