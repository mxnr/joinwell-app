<?php

namespace ZoomBundle\Packet\User;

use ZoomBundle\Contract\RequestPacket;

/**
 * Class DeletePermanently
 * @package ZoomBundle\Packet\User
 */
class DeletePermanently implements RequestPacket
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $email;

    /**
     * @return string
     */
    public function getPath(): string
    {
        return 'user/permanentdelete';
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        $params = [
            'id' => $this->getId(),
        ];

        if ($this->hasEmail()) {
            $params['email'] = $this->getEmail();
        }

        return $params;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function hasId(): bool
    {
        return !empty($this->id);
    }

    /**
     * @param string $id
     *
     * @return DeletePermanently
     */
    public function setId(string $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return bool
     */
    public function hasEmail(): bool
    {
        return !empty($this->email);
    }

    /**
     * @param string $email
     *
     * @return DeletePermanently
     */
    public function setEmail(string $email)
    {
        $this->email = $email;

        return $this;
    }
}
