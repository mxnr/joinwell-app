<?php

namespace ZoomBundle\Packet\User;

use ZoomBundle\Contract\RequestPacket;

/**
 * Class UpdatePassword
 * @package ZoomBundle\Packet\User
 */
class UpdatePassword implements RequestPacket
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $password;

    /**
     * @return string
     */
    public function getPath(): string
    {
        return 'user/updatepassword';
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return [
            'id' => $this->getId(),
            'password' => $this->getPassword(),
        ];
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function hasId(): bool
    {
        return !empty($this->id);
    }

    /**
     * @param string $id
     *
     * @return UpdatePassword
     */
    public function setId(string $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @return bool
     */
    public function hasPassword(): bool
    {
        return !empty($this->password);
    }

    /**
     * @param string $password
     *
     * @return UpdatePassword
     */
    public function setPassword(string $password)
    {
        $this->password = $password;

        return $this;
    }
}
