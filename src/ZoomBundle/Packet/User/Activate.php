<?php

namespace ZoomBundle\Packet\User;

use ZoomBundle\Contract\RequestPacket;

/**
 * Class Activate
 * @package ZoomBundle\Packet\User
 */
class Activate implements RequestPacket
{
    /**
     * @var string
     */
    private $id;

    /**
     * @return string
     */
    public function getPath(): string
    {
        return 'user/activate';
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return [
            'id' => $this->getId(),
        ];
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function hasId(): bool
    {
        return !empty($this->id);
    }

    /**
     * @param string $id
     *
     * @return Activate
     */
    public function setId(string $id)
    {
        $this->id = $id;

        return $this;
    }
}
