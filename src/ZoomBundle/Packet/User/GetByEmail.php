<?php

namespace ZoomBundle\Packet\User;

use ZoomBundle\Contract\RequestPacket;

/**
 * Class GetByEmail
 * @package ZoomBundle\Packet\User
 */
class GetByEmail implements RequestPacket
{
    const SNS_GOOGLE = 1;

    const SNS_API = 99;

    const SNS_ZOOM = 100;

    const SNS_SSO = 101;

    /**
     * @var string
     */
    private $email;

    /**
     * @var int
     */
    private $loginType;

    /**
     * @return string
     */
    public function getPath(): string
    {
        return 'user/getbyemail';
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        $params = [];
        if ($this->hasEmail()) {
            $params['email'] = $this->getEmail();
        }

        if ($this->hasLoginType()) {
            $params['login_type'] = $this->getLoginType();
        }


        return $params;
    }

    /**
     * @return bool
     */
    public function hasEmail(): bool
    {
        return !empty($this->email);
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return GetByEmail
     */
    public function setEmail(string $email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasLoginType(): bool
    {
        return !empty($this->loginType);
    }

    /**
     * @return int
     */
    public function getLoginType(): int
    {
        return $this->loginType;
    }

    /**
     * @param int $loginType
     *
     * @return GetByEmail
     */
    public function setLoginType(int $loginType)
    {
        if (in_array(
                $loginType,
                [self::SNS_API, self::SNS_GOOGLE, self::SNS_SSO, self::SNS_ZOOM]
            ) === false
        ) {
            throw new \InvalidArgumentException(
                sprintf(
                    '$loginType must be one of [ %s ] values',
                    implode(
                        ', ',
                        [
                            self::SNS_API,
                            self::SNS_GOOGLE,
                            self::SNS_SSO,
                            self::SNS_ZOOM,
                        ]
                    )
                )
            );
        }

        $this->loginType = $loginType;

        return $this;
    }
}
