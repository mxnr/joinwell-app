<?php

namespace ZoomBundle\Packet\User;

use ZoomBundle\Contract\RequestPacket;

/**
 * Class Create
 * @package ZoomBundle\Packet\User
 */
class Create implements RequestPacket
{
    const TYPE_BASIC = 1;

    const TYPE_PRO = 2;

    const TYPE_CORP = 3;

    /**
     * first_name
     * optional
     * User’s first name
     *
     * @var string
     */
    private $firstName;

    /**
     * last_name
     * optional
     * User’s last name
     *
     * @var string
     */
    private $lastName;

    /**
     * disable_chat
     * optional
     * Disable in-meeting chat
     * Deafult: false
     *
     * @var bool
     */
    private $disableChat = false;

    /**
     * enable_e2e_encryption
     * optional
     * Enable end-to-end encryption
     * Deafult: false
     *
     * @var bool
     */
    private $enableE2EEncryption = false;

    /**
     * enable_silent_mode
     * optional
     * Enable attendee on-hold
     * Deafult: false
     *
     * @var bool
     */
    private $enableSilentMode = false;

    /**
     * disable_group_hd
     * optional
     * Disable Group HD
     * Deafult: false
     *
     * @var bool
     */
    private $disableGroupHd = false;

    /**
     * disable_recording
     * optional
     * Disable recording
     * Deafult: false
     *
     * @var bool
     */
    private $disableRecording = false;

    /**
     * enable_cmr
     * optional
     * Enable cloud meeting record
     * Deafult: false
     *
     * @var bool
     */
    private $enableCmr = false;

    /**
     * enable_auto_recording
     * optional
     * Enable automatic recording. By default, if “enable_cloud_auto_recording” is true, it will become true
     *
     * Deafult: false
     *
     * @var bool
     */
    private $enableAutoRecording = false;

    /**
     * enable_cloud_auto_recording
     * optional
     * Enable cloud automatic recording
     * Deafult: false
     *
     * @var bool
     */
    private $enableCloudAutoRecordong = false;

    /**
     * track_id
     * optional
     * Referral partner track ID
     *
     * @var string
     */
    private $trackId;

    /**
     * meeting_capacity
     * optional
     * User’s meeting capacity, passing 0 will inherit account’s meeting capacity
     * Deafult: 0
     *
     * @var int
     */
    private $meetingCapacity = 0;

    /**
     * enable_webinar
     * optional
     * Enable webinar feature
     *
     * @var bool
     */
    private $enableWebinar;

    /**
     * webinar_capacity
     * optional
     * Enable webinar capacity, can be 100, 500, 1000, 3000, 5000 or 10000, depends on if having related webinar
     * capacity plan subscription or not
     * Deafult: 0
     *
     * @var int
     */
    private $webinarCapacity = 0;

    /**
     * Enable large meting feature
     *
     * @var bool
     */
    private $enableLarge;

    /**
     * Enable large meeting capacity, can be 100, 200, 300 or 500, depends on if having related webinar capacity plan
     * subscription or not Deafult: 0
     *
     * @var int
     */
    private $largeCapacity = 0;

    /**
     * Disable feedback
     * Deafult: false
     *
     * @var bool
     */
    private $disableFeedback = false;

    /**
     * Disable jbh email reminder
     * Deafult: false
     *
     * @var bool
     */
    private $disableJbhReminder = false;

    /**
     * Enable breakout room
     * Deafult: false
     *
     * @var bool
     */
    private $enableBreakoutRoom = false;

    /**
     * Enable polling for meeting
     * Deafult: false
     *
     * @var bool
     */
    private $enablePolling = false;

    /**
     * Enable Annotation
     * Deafult: true
     *
     * @var bool
     */
    private $enable_annotation = true;

    /**
     * Enable Share dual camera
     * Deafult: false
     *
     * @var bool
     */
    private $enableShareDualCamera = false;

    /**
     * Enable Far end camera control
     * Deafult: false
     *
     * @var bool
     */
    private $enableFarEndCameraControl = false;

    /**
     * Disable Private chat
     * Deafult: false
     *
     * @var bool
     */
    private $disablePrivateChat = false;

    /**
     * Enable Enter/exit chime
     * Deafult: false
     *
     * @var bool
     */
    private $enableEnterExitChime = false;

    /**
     * Enter/exit chime type. 0 means Heard by all including host and attendees, 1 means Heard by host only
     * Deafult: 0
     *
     * @var int
     */
    private $optionEnterExitChimeType = 0;

    /**
     * Disable email notification when a meeting is cancelled
     * Deafult: false
     *
     * @var bool
     */
    private $disableCancelMeetingNotification = false;

    /**
     * Enable Remote support
     * Deafult: false
     *
     * @var bool
     */
    private $enableRemoteSupport = false;

    /**
     * Enable File transfer
     * Deafult: false
     *
     * @var bool
     */
    private $enableFileTransfer = false;

    /**
     * Enable Virtual background
     * Deafult: false
     *
     * @var bool
     */
    private $enableVirtualBackground = false;

    /**
     * Enable Attention tracking
     * Deafult: false
     *
     * @var bool
     */
    private $enableAttentionTracking = false;

    /**
     * Enable Waiting room
     * Deafult: false
     *
     * @var bool
     */
    private $enableWaitingRoom = false;

    /**
     * Enable Closed caption
     * Deafult: false
     *
     * @var bool
     */
    private $enableClosedCaption = false;

    /**
     * Enable Co-host
     * Deafult: false
     *
     * @var bool
     */
    private $enableCoHost = false;

    /**
     * Enable Auto saving chats
     * Deafult: false
     *
     * @var bool
     */
    private $enableAutoSavingChats = false;

    /**
     * Enable Generate and require password for participants joining by phone
     * Deafult: false
     *
     * @var bool
     */
    private $enablePhoneParticipantsPassword = false;

    /**
     * Enable Auto delete cloud recordings
     * Deafult: false
     *
     * @var bool
     */
    private $enableAutoDeleteCmr = false;

    /**
     * Auto delete cloud recordings after days
     *
     * @var int
     */
    private $autoDeleteCmrDays;

    /**
     * Department for user profile, use for repor
     *
     * @var string
     */
    private $dept;

    /**
     * User Group ID. If set default user group, the parameter’s default value is the default user group
     *
     * @var string
     */
    private $groupId;

    /**
     * IM Group ID. If set default im group, the parameter’s default value is the default im group
     *
     * @var string
     */
    private $imGroupId;

    /**
     * @var string
     */
    private $email;

    /**
     * @var int
     */
    private $type;

    /**
     * @return string
     */
    public function getPath(): string
    {
        return 'user/create';
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        $params = [
            'email' => $this->getEmail(),
            'type' => $this->getType(),
        ];

        if ($this->hasFirstName()) {
            $params['first_name'] = $this->getFirstName();
        }

        if ($this->getLastName()) {
            $params['last_name'] = $this->getLastName();
        }

        if ($this->hasDisableChat()) {
            $params['disable_chat'] = $this->isDisableChat();
        }

        if ($this->hasEnableE2EEncryption()) {
            $params['enable_e2e_encryption'] = $this->isEnableE2EEncryption();
        }

        if ($this->hasEnableSilentMode()) {
            $params['enable_silent_mode'] = $this->isEnableSilentMode();
        }

        if ($this->hasDisableGroupHd()) {
            $params['disable_group_hd'] = $this->isDisableGroupHd();
        }

        if ($this->hasDisableRecording()) {
            $params['disable_recording'] = $this->isDisableRecording();
        }

        if ($this->hasEnableCmr()) {
            $params['enable_cmr'] = $this->isEnableCmr();
        }

        if ($this->hasEnableAutoRecording()) {
            $params['enable_auto_recording'] = $this->isEnableAutoRecording();
        }

        if ($this->hasEnableCloudAutoRecordong()) {
            $params['enable_cloud_auto_recording'] = $this->isEnableCloudAutoRecordong();
        }

        if ($this->hasTrackId()) {
            $params['track_id'] = $this->getTrackId();
        }

        if ($this->hasMeetingCapacity()) {
            $params['meeting_capacity'] = $this->getMeetingCapacity();
        }

        if ($this->hasEnableWebinar()) {
            $params['enable_webinar'] = $this->isEnableWebinar();
        }

        if ($this->hasWebinarCapacity()) {
            $params['webinar_capacity'] = $this->getWebinarCapacity();
        }

        if ($this->hasEnableLarge()) {
            $params['enable_large'] = $this->isEnableLarge();
        }

        if ($this->hasLargeCapacity()) {
            $params['large_capacity'] = $this->getLargeCapacity();
        }

        if ($this->hasDisableFeedback()) {
            $params['disable_feedback'] = $this->isDisableFeedback();
        }

        if ($this->hasDisableJbhReminder()) {
            $params['disable_jbh_reminder'] = $this->isDisableJbhReminder();
        }

        if ($this->hasEnableBreakoutRoom()) {
            $params['enable_breakout_room'] = $this->isEnableBreakoutRoom();
        }

        if ($this->hasEnablePolling()) {
            $params['enable_polling'] = $this->isEnablePolling();
        }

        if ($this->hasEnableAnnotation()) {
            $params['enable_annotation'] = $this->isEnableAnnotation();
        }

        if ($this->hasEnableShareDualCamera()) {
            $params['enable_share_dual_camera'] = $this->isEnableShareDualCamera();
        }

        if ($this->hasEnableFarEndCameraControl()) {
            $params['enable_far_end_camera_control'] = $this->isEnableFarEndCameraControl();
        }

        if ($this->hasDisablePrivateChat()) {
            $params['disable_private_chat'] = $this->isDisablePrivateChat();
        }

        if ($this->hasEnableEnterExitChime()) {
            $params['enable_enter_exit_chime'] = $this->isEnableEnterExitChime();
        }

        if ($this->hasOptionEnterExitChimeType()) {
            $params['option_enter_exit_chime_type'] = $this->getOptionEnterExitChimeType();
        }

        if ($this->hasDisableCancelMeetingNotification()) {
            $params['disable_cancel_meeting_notification'] = $this->isDisableCancelMeetingNotification();
        }

        if ($this->hasEnableRemoteSupport()) {
            $params['enable_remote_support'] = $this->isEnableRemoteSupport();
        }

        if ($this->hasEnableFileTransfer()) {
            $params['enable_file_transfer'] = $this->isEnableFileTransfer();
        }

        if ($this->hasEnableVirtualBackground()) {
            $params['enable_virtual_background'] = $this->isEnableVirtualBackground();
        }

        if ($this->hasEnableAttentionTracking()) {
            $params['enable_attention_tracking'] = $this->isEnableAttentionTracking();
        }

        if ($this->hasEnableWaitingRoom()) {
            $params['enable_waiting_room'] = $this->isEnableWaitingRoom();
        }

        if ($this->hasEnableClosedCaption()) {
            $params['enable_closed_caption'] = $this->isEnableClosedCaption();
        }

        if ($this->hasEnableCoHost()) {
            $params['enable_co_host'] = $this->isEnableCoHost();
        }

        if ($this->hasEnableAutoSavingChats()) {
            $params['enable_auto_saving_chats'] = $this->isEnableAutoSavingChats();
        }

        if ($this->hasEnablePhoneParticipantsPassword()) {
            $params['enable_phone_participants_password'] = $this->isEnablePhoneParticipantsPassword();
        }

        if ($this->hasEnableAutoDeleteCmr()) {
            $params['enable_auto_delete_cmr'] = $this->isEnableAutoDeleteCmr();
        }

        if ($this->hasAutoDeleteCmrDays()) {
            $params['auto_delete_cmr_days'] = $this->getAutoDeleteCmrDays();
        }

        if ($this->hasDept()) {
            $params['dept'] = $this->getDept();
        }

        if ($this->hasGroupId()) {
            $params['group_id'] = $this->getGroupId();
        }

        if ($this->hasImGroupId()) {
            $params['imgroup_id'] = $this->getImGroupId();
        }

        return $params;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return bool
     */
    public function hasEmail(): bool
    {
        return !empty($this->email);
    }

    /**
     * @param string $email
     *
     * @return Create
     */
    public function setEmail(string $email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @return bool
     */
    public function hasType(): bool
    {
        return !empty($this->type);
    }

    /**
     * @param int $type
     *
     * @return Create
     */
    public function setType(int $type)
    {
        if (in_array($type, [self::TYPE_BASIC, self::TYPE_PRO, self::TYPE_CORP]) === false) {
            throw new \InvalidArgumentException(
                '$type must be one of [ %s ] values',
                implode(', ', [self::TYPE_BASIC, self::TYPE_PRO, self::TYPE_CORP])
            );
        }

        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return bool
     */
    public function hasFirstName(): bool
    {
        return !empty($this->firstName);
    }

    /**
     * @param string $firstName
     *
     * @return Create
     */
    public function setFirstName(string $firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @return bool
     */
    public function hasLastName(): bool
    {
        return !empty($this->lastName);
    }

    /**
     * @param string $lastName
     *
     * @return Create
     */
    public function setLastName(string $lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDisableChat(): bool
    {
        return $this->disableChat;
    }

    /**
     * @return bool
     */
    public function hasDisableChat(): bool
    {
        return !empty($this->disableChat);
    }

    /**
     * @param bool $disableChat
     *
     * @return Create
     */
    public function setDisableChat(bool $disableChat)
    {
        $this->disableChat = $disableChat;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEnableE2EEncryption(): bool
    {
        return $this->enableE2EEncryption;
    }

    /**
     * @return bool
     */
    public function hasEnableE2EEncryption(): bool
    {
        return !empty($this->enableE2EEncryption);
    }

    /**
     * @param bool $enableE2EEncryption
     *
     * @return Create
     */
    public function setEnableE2EEncryption(bool $enableE2EEncryption)
    {
        $this->enableE2EEncryption = $enableE2EEncryption;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEnableSilentMode(): bool
    {
        return $this->enableSilentMode;
    }

    /**
     * @return bool
     */
    public function hasEnableSilentMode(): bool
    {
        return !empty($this->enableSilentMode);
    }

    /**
     * @param bool $enableSilentMode
     *
     * @return Create
     */
    public function setEnableSilentMode(bool $enableSilentMode)
    {
        $this->enableSilentMode = $enableSilentMode;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDisableGroupHd(): bool
    {
        return $this->disableGroupHd;
    }

    /**
     * @return bool
     */
    public function hasDisableGroupHd(): bool
    {
        return !empty($this->disableGroupHd);
    }

    /**
     * @param bool $disableGroupHd
     *
     * @return Create
     */
    public function setDisableGroupHd(bool $disableGroupHd)
    {
        $this->disableGroupHd = $disableGroupHd;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDisableRecording(): bool
    {
        return $this->disableRecording;
    }

    /**
     * @return bool
     */
    public function hasDisableRecording(): bool
    {
        return !empty($this->disableRecording);
    }

    /**
     * @param bool $disableRecording
     *
     * @return Create
     */
    public function setDisableRecording(bool $disableRecording)
    {
        $this->disableRecording = $disableRecording;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEnableCmr(): bool
    {
        return $this->enableCmr;
    }

    /**
     * @return bool
     */
    public function hasEnableCmr(): bool
    {
        return !empty($this->enableCmr);
    }

    /**
     * @param bool $enableCmr
     *
     * @return Create
     */
    public function setEnableCmr(bool $enableCmr)
    {
        $this->enableCmr = $enableCmr;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEnableAutoRecording(): bool
    {
        return $this->enableAutoRecording;
    }

    /**
     * @return bool
     */
    public function hasEnableAutoRecording(): bool
    {
        return !empty($this->enableAutoRecording);
    }

    /**
     * @param bool $enableAutoRecording
     *
     * @return Create
     */
    public function setEnableAutoRecording(bool $enableAutoRecording)
    {
        $this->enableAutoRecording = $enableAutoRecording;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEnableCloudAutoRecordong(): bool
    {
        return $this->enableCloudAutoRecordong;
    }

    /**
     * @return bool
     */
    public function hasEnableCloudAutoRecordong(): bool
    {
        return !empty($this->enableCloudAutoRecordong);
    }

    /**
     * @param bool $enableCloudAutoRecordong
     *
     * @return Create
     */
    public function setEnableCloudAutoRecordong(bool $enableCloudAutoRecordong)
    {
        $this->enableCloudAutoRecordong = $enableCloudAutoRecordong;

        return $this;
    }

    /**
     * @return string
     */
    public function getTrackId(): string
    {
        return $this->trackId;
    }

    /**
     * @return bool
     */
    public function hasTrackId(): bool
    {
        return !empty($this->trackId);
    }

    /**
     * @param string $trackId
     *
     * @return Create
     */
    public function setTrackId(string $trackId)
    {
        $this->trackId = $trackId;

        return $this;
    }

    /**
     * @return int
     */
    public function getMeetingCapacity(): int
    {
        return $this->meetingCapacity;
    }

    /**
     * @return bool
     */
    public function hasMeetingCapacity(): bool
    {
        return !empty($this->meetingCapacity);
    }

    /**
     * @param int $meetingCapacity
     *
     * @return Create
     */
    public function setMeetingCapacity(int $meetingCapacity)
    {
        $this->meetingCapacity = $meetingCapacity;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEnableWebinar(): bool
    {
        return $this->enableWebinar;
    }

    /**
     * @return bool
     */
    public function hasEnableWebinar(): bool
    {
        return !empty($this->enableWebinar);
    }

    /**
     * @param bool $enableWebinar
     *
     * @return Create
     */
    public function setEnableWebinar(bool $enableWebinar)
    {
        $this->enableWebinar = $enableWebinar;

        return $this;
    }

    /**
     * @return int
     */
    public function getWebinarCapacity(): int
    {
        return $this->webinarCapacity;
    }

    /**
     * @return bool
     */
    public function hasWebinarCapacity(): bool
    {
        return !empty($this->webinarCapacity);
    }

    /**
     * @param int $webinarCapacity
     *
     * @return Create
     */
    public function setWebinarCapacity(int $webinarCapacity)
    {
        $this->webinarCapacity = $webinarCapacity;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEnableLarge(): bool
    {
        return $this->enableLarge;
    }

    /**
     * @return bool
     */
    public function hasEnableLarge(): bool
    {
        return !empty($this->enableLarge);
    }

    /**
     * @param bool $enableLarge
     *
     * @return Create
     */
    public function setEnableLarge(bool $enableLarge)
    {
        $this->enableLarge = $enableLarge;

        return $this;
    }

    /**
     * @return int
     */
    public function getLargeCapacity(): int
    {
        return $this->largeCapacity;
    }

    /**
     * @return bool
     */
    public function hasLargeCapacity(): bool
    {
        return !empty($this->largeCapacity);
    }

    /**
     * @param int $largeCapacity
     *
     * @return Create
     */
    public function setLargeCapacity(int $largeCapacity)
    {
        $this->largeCapacity = $largeCapacity;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDisableFeedback(): bool
    {
        return $this->disableFeedback;
    }

    /**
     * @return bool
     */
    public function hasDisableFeedback(): bool
    {
        return !empty($this->disableFeedback);
    }

    /**
     * @param bool $disableFeedback
     *
     * @return Create
     */
    public function setDisableFeedback(bool $disableFeedback)
    {
        $this->disableFeedback = $disableFeedback;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDisableJbhReminder(): bool
    {
        return $this->disableJbhReminder;
    }

    /**
     * @return bool
     */
    public function hasDisableJbhReminder(): bool
    {
        return !empty($this->disableJbhReminder);
    }

    /**
     * @param bool $disableJbhReminder
     *
     * @return Create
     */
    public function setDisableJbhReminder(bool $disableJbhReminder)
    {
        $this->disableJbhReminder = $disableJbhReminder;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEnableBreakoutRoom(): bool
    {
        return $this->enableBreakoutRoom;
    }

    /**
     * @return bool
     */
    public function hasEnableBreakoutRoom(): bool
    {
        return !empty($this->enableBreakoutRoom);
    }

    /**
     * @param bool $enableBreakoutRoom
     *
     * @return Create
     */
    public function setEnableBreakoutRoom(bool $enableBreakoutRoom)
    {
        $this->enableBreakoutRoom = $enableBreakoutRoom;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEnablePolling(): bool
    {
        return $this->enablePolling;
    }

    /**
     * @return bool
     */
    public function hasEnablePolling(): bool
    {
        return !empty($this->enablePolling);
    }

    /**
     * @param bool $enablePolling
     *
     * @return Create
     */
    public function setEnablePolling(bool $enablePolling)
    {
        $this->enablePolling = $enablePolling;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEnableAnnotation(): bool
    {
        return $this->enable_annotation;
    }

    /**
     * @return bool
     */
    public function hasEnableAnnotation(): bool
    {
        return !empty($this->enable_annotation);
    }

    /**
     * @param bool $enable_annotation
     *
     * @return Create
     */
    public function setEnableAnnotation(bool $enable_annotation)
    {
        $this->enable_annotation = $enable_annotation;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEnableShareDualCamera(): bool
    {
        return $this->enableShareDualCamera;
    }

    /**
     * @return bool
     */
    public function hasEnableShareDualCamera(): bool
    {
        return !empty($this->enableShareDualCamera);
    }

    /**
     * @param bool $enableShareDualCamera
     *
     * @return Create
     */
    public function setEnableShareDualCamera(bool $enableShareDualCamera)
    {
        $this->enableShareDualCamera = $enableShareDualCamera;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEnableFarEndCameraControl(): bool
    {
        return $this->enableFarEndCameraControl;
    }

    /**
     * @return bool
     */
    public function hasEnableFarEndCameraControl(): bool
    {
        return !empty($this->enableFarEndCameraControl);
    }

    /**
     * @param bool $enableFarEndCameraControl
     *
     * @return Create
     */
    public function setEnableFarEndCameraControl(bool $enableFarEndCameraControl)
    {
        $this->enableFarEndCameraControl = $enableFarEndCameraControl;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDisablePrivateChat(): bool
    {
        return $this->disablePrivateChat;
    }

    /**
     * @return bool
     */
    public function hasDisablePrivateChat(): bool
    {
        return !empty($this->disablePrivateChat);
    }

    /**
     * @param bool $disablePrivateChat
     *
     * @return Create
     */
    public function setDisablePrivateChat(bool $disablePrivateChat)
    {
        $this->disablePrivateChat = $disablePrivateChat;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEnableEnterExitChime(): bool
    {
        return $this->enableEnterExitChime;
    }

    /**
     * @return bool
     */
    public function hasEnableEnterExitChime(): bool
    {
        return !empty($this->enableEnterExitChime);
    }

    /**
     * @param bool $enableEnterExitChime
     *
     * @return Create
     */
    public function setEnableEnterExitChime(bool $enableEnterExitChime)
    {
        $this->enableEnterExitChime = $enableEnterExitChime;

        return $this;
    }

    /**
     * @return int
     */
    public function getOptionEnterExitChimeType(): int
    {
        return $this->optionEnterExitChimeType;
    }

    /**
     * @return bool
     */
    public function hasOptionEnterExitChimeType(): bool
    {
        return !empty($this->optionEnterExitChimeType);
    }

    /**
     * @param int $optionEnterExitChimeType
     *
     * @return Create
     */
    public function setOptionEnterExitChimeType(int $optionEnterExitChimeType)
    {
        $this->optionEnterExitChimeType = $optionEnterExitChimeType;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDisableCancelMeetingNotification(): bool
    {
        return $this->disableCancelMeetingNotification;
    }

    /**
     * @return bool
     */
    public function hasDisableCancelMeetingNotification(): bool
    {
        return !empty($this->disableCancelMeetingNotification);
    }

    /**
     * @param bool $disableCancelMeetingNotification
     *
     * @return Create
     */
    public function setDisableCancelMeetingNotification(bool $disableCancelMeetingNotification)
    {
        $this->disableCancelMeetingNotification = $disableCancelMeetingNotification;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEnableRemoteSupport(): bool
    {
        return $this->enableRemoteSupport;
    }

    /**
     * @return bool
     */
    public function hasEnableRemoteSupport(): bool
    {
        return !empty($this->enableRemoteSupport);
    }

    /**
     * @param bool $enableRemoteSupport
     *
     * @return Create
     */
    public function setEnableRemoteSupport(bool $enableRemoteSupport)
    {
        $this->enableRemoteSupport = $enableRemoteSupport;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEnableFileTransfer(): bool
    {
        return $this->enableFileTransfer;
    }

    /**
     * @return bool
     */
    public function hasEnableFileTransfer(): bool
    {
        return !empty($this->enableFileTransfer);
    }

    /**
     * @param bool $enableFileTransfer
     *
     * @return Create
     */
    public function setEnableFileTransfer(bool $enableFileTransfer)
    {
        $this->enableFileTransfer = $enableFileTransfer;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEnableVirtualBackground(): bool
    {
        return $this->enableVirtualBackground;
    }

    /**
     * @return bool
     */
    public function hasEnableVirtualBackground(): bool
    {
        return !empty($this->enableVirtualBackground);
    }

    /**
     * @param bool $enableVirtualBackground
     *
     * @return Create
     */
    public function setEnableVirtualBackground(bool $enableVirtualBackground)
    {
        $this->enableVirtualBackground = $enableVirtualBackground;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEnableAttentionTracking(): bool
    {
        return $this->enableAttentionTracking;
    }

    /**
     * @return bool
     */
    public function hasEnableAttentionTracking(): bool
    {
        return !empty($this->enableAttentionTracking);
    }

    /**
     * @param bool $enableAttentionTracking
     *
     * @return Create
     */
    public function setEnableAttentionTracking(bool $enableAttentionTracking)
    {
        $this->enableAttentionTracking = $enableAttentionTracking;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEnableWaitingRoom(): bool
    {
        return $this->enableWaitingRoom;
    }

    /**
     * @return bool
     */
    public function hasEnableWaitingRoom(): bool
    {
        return !empty($this->enableWaitingRoom);
    }

    /**
     * @param bool $enableWaitingRoom
     *
     * @return Create
     */
    public function setEnableWaitingRoom(bool $enableWaitingRoom)
    {
        $this->enableWaitingRoom = $enableWaitingRoom;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEnableClosedCaption(): bool
    {
        return $this->enableClosedCaption;
    }

    /**
     * @return bool
     */
    public function hasEnableClosedCaption(): bool
    {
        return !empty($this->enableClosedCaption);
    }

    /**
     * @param bool $enableClosedCaption
     *
     * @return Create
     */
    public function setEnableClosedCaption(bool $enableClosedCaption)
    {
        $this->enableClosedCaption = $enableClosedCaption;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEnableCoHost(): bool
    {
        return $this->enableCoHost;
    }

    /**
     * @return bool
     */
    public function hasEnableCoHost(): bool
    {
        return !empty($this->enableCoHost);
    }

    /**
     * @param bool $enableCoHost
     *
     * @return Create
     */
    public function setEnableCoHost(bool $enableCoHost)
    {
        $this->enableCoHost = $enableCoHost;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEnableAutoSavingChats(): bool
    {
        return $this->enableAutoSavingChats;
    }

    /**
     * @return bool
     */
    public function hasEnableAutoSavingChats(): bool
    {
        return !empty($this->enableAutoSavingChats);
    }

    /**
     * @param bool $enableAutoSavingChats
     *
     * @return Create
     */
    public function setEnableAutoSavingChats(bool $enableAutoSavingChats)
    {
        $this->enableAutoSavingChats = $enableAutoSavingChats;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEnablePhoneParticipantsPassword(): bool
    {
        return $this->enablePhoneParticipantsPassword;
    }

    /**
     * @return bool
     */
    public function hasEnablePhoneParticipantsPassword(): bool
    {
        return !empty($this->enablePhoneParticipantsPassword);
    }

    /**
     * @param bool $enablePhoneParticipantsPassword
     *
     * @return Create
     */
    public function setEnablePhoneParticipantsPassword(bool $enablePhoneParticipantsPassword)
    {
        $this->enablePhoneParticipantsPassword = $enablePhoneParticipantsPassword;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEnableAutoDeleteCmr(): bool
    {
        return $this->enableAutoDeleteCmr;
    }

    /**
     * @return bool
     */
    public function hasEnableAutoDeleteCmr(): bool
    {
        return !empty($this->enableAutoDeleteCmr);
    }

    /**
     * @param bool $enableAutoDeleteCmr
     *
     * @return Create
     */
    public function setEnableAutoDeleteCmr(bool $enableAutoDeleteCmr)
    {
        $this->enableAutoDeleteCmr = $enableAutoDeleteCmr;

        return $this;
    }

    /**
     * @return int
     */
    public function getAutoDeleteCmrDays(): int
    {
        return $this->autoDeleteCmrDays;
    }

    /**
     * @return bool
     */
    public function hasAutoDeleteCmrDays(): bool
    {
        return !empty($this->autoDeleteCmrDays);
    }

    /**
     * @param int $autoDeleteCmrDays
     *
     * @return Create
     */
    public function setAutoDeleteCmrDays(int $autoDeleteCmrDays)
    {
        $this->autoDeleteCmrDays = $autoDeleteCmrDays;

        return $this;
    }

    /**
     * @return string
     */
    public function getDept(): string
    {
        return $this->dept;
    }

    /**
     * @return bool
     */
    public function hasDept(): bool
    {
        return !empty($this->dept);
    }

    /**
     * @param string $dept
     *
     * @return Create
     */
    public function setDept(string $dept)
    {
        $this->dept = $dept;

        return $this;
    }

    /**
     * @return string
     */
    public function getGroupId(): string
    {
        return $this->groupId;
    }

    /**
     * @return bool
     */
    public function hasGroupId(): bool
    {
        return !empty($this->groupId);
    }

    /**
     * @param string $groupId
     *
     * @return Create
     */
    public function setGroupId(string $groupId)
    {
        $this->groupId = $groupId;

        return $this;
    }

    /**
     * @return string
     */
    public function getImGroupId(): string
    {
        return $this->imGroupId;
    }

    /**
     * @return bool
     */
    public function hasImGroupId(): bool
    {
        return !empty($this->imGroupId);
    }

    /**
     * @param string $imGroupId
     *
     * @return Create
     */
    public function setImGroupId(string $imGroupId)
    {
        $this->imGroupId = $imGroupId;

        return $this;
    }
}
