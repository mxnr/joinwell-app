<?php

namespace ZoomBundle\Packet\User;

use ZoomBundle\Contract\RequestPacket;

/**
 * Class GetList
 * @package ZoomBundle\Entity\User
 */
class GetList implements RequestPacket
{
    /**
     * @var int
     */
    private $pageSize = 20;

    /**
     * @var int
     */
    private $pageNumber = 1;

    /**
     * @return string
     */
    public function getPath(): string
    {
        return 'user/list';
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return [
            'page_size' => $this->getPageSize(),
            'page_number' => $this->getPageNumber(),
        ];
    }

    /**
     * @return int
     */
    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * @param int $pageSize
     *
     * @return GetList
     */
    public function setPageSize(int $pageSize)
    {
        $this->pageSize = $pageSize;

        return $this;
    }

    /**
     * @return int
     */
    public function getPageNumber(): int
    {
        return $this->pageNumber;
    }

    /**
     * @param int $pageNumber
     *
     * @return GetList
     */
    public function setPageNumber(int $pageNumber)
    {
        $this->pageNumber = $pageNumber;

        return $this;
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        return [];
    }
}
