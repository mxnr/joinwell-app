<?php

namespace ZoomBundle\Packet\User;

/**
 * Class CreateAuto
 * @package ZoomBundle\Packet\User
 */
class CreateAuto extends Create
{
    /**
     * @return string
     */
    public function getPath(): string
    {
        return 'user/autocreate';
    }
}
