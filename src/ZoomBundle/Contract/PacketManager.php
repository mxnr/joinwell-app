<?php

namespace ZoomBundle\Contract;

/**
 * Interface PacketManager
 * @package ZoomBundle\Contract
 */
interface PacketManager
{
    /**
     * @param RequestPacket $packet
     *
     * @return array
     */
    public function flush(RequestPacket $packet): array;

    /**
     * @return RequestHandler
     */
    public function getRequestHandler(): RequestHandler;

    /**
     * @return RequestHandlerConfigurable
     */
    public function getRequestHandlerConfigurator(): RequestHandlerConfigurable;
}
