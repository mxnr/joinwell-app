<?php

namespace ZoomBundle\Contract;

/**
 * Interface RequestPacket
 * @package ZoomBundle\Contract
 */
interface RequestPacket
{
    /**
     * @return string
     */
    public function getPath(): string;

    /**
     * @return array
     */
    public function getParams(): array;
}
