<?php

namespace ZoomBundle\Contract;

/**
 * Interface RequestHandler
 * @package ZoomBundle\Contract
 */
interface RequestHandler
{
    /**
     * @param string $path
     * @param array $parameters
     *
     * @return array
     */
    public function perform(string $path, array $parameters = []): array;
}
