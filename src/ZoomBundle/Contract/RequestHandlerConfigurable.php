<?php


namespace ZoomBundle\Contract;


/**
 * Interface RequestHandlerConfigurator
 * @package ZoomBundle\Contract
 */
interface RequestHandlerConfigurable
{
    /**
     * @return bool
     */
    public function getDebug(): bool;

    /**
     * @param bool $debug
     *
     * @return RequestHandlerConfigurable
     */
    public function setDebug(bool $debug): RequestHandlerConfigurable;

    /**
     * @return int
     */
    public function getTimeout(): int;

    /**
     * @param int $timeout
     *
     * @return RequestHandlerConfigurable
     */
    public function setTimeout(int $timeout): RequestHandlerConfigurable;
}
