<?php

namespace ZoomBundle\Manager;

use ZoomBundle\Contract\PacketManager;
use ZoomBundle\Contract\RequestHandler;
use ZoomBundle\Contract\RequestHandlerConfigurable;
use ZoomBundle\Contract\RequestPacket;

/**
 * Class ApiPacketManager
 * @package ZoomBundle\Manager
 */
class ApiPacketManager implements PacketManager
{
    private $request;

    /**
     * ApiPacketManager constructor.
     *
     * @param RequestHandler $handler
     */
    public function __construct(RequestHandler $handler)
    {
        $this->request = $handler;
    }

    /**
     * @param RequestPacket $packet
     *
     * @return array
     */
    public function flush(RequestPacket $packet): array
    {
        return $this->request->perform($packet->getPath(), $packet->getParams());
    }

    /**
     * @return bool
     */
    public function hasRequest(): bool
    {
        return !empty($this->request);
    }


    /**
     * @return RequestHandler
     */
    public function getRequestHandler(): RequestHandler
    {
        return $this->request;
    }

    /**
     * @return RequestHandlerConfigurable
     */
    public function getRequestHandlerConfigurator(): RequestHandlerConfigurable
    {
        return $this->request;
    }
}
