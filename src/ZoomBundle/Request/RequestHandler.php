<?php

namespace ZoomBundle\Request;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use ZoomBundle\Contract\RequestHandler as RequestInterface;
use ZoomBundle\Contract\RequestHandlerConfigurable;
use ZoomBundle\Exception\ZoomApiCallFailedException;
use ZoomBundle\Exception\ZoomApiCallTimedOutException;
use ZoomBundle\Exception\ZoomApiErrorException;

/**
 * Class RequestHandler
 * @package ZoomBundle\Request
 */
class RequestHandler implements RequestInterface, RequestHandlerConfigurable
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    private $url = 'https://api.zoom.us';

    /**
     * @var string
     */
    private $versionUri = 'v%d';

    /**
     * @var int
     */
    private $version = 1;

    /**
     * @var null|string
     */
    private $key;

    /**
     * @var null|string
     */
    private $secret;

    /**
     * @var null|string
     */
    private $token;

    /**
     * @var int
     */
    private $timeout = 30;

    /**
     * @var string json|xml
     */
    private $dataType = 'json';

    /**
     * @var string POST|GET
     */
    private $method = 'POST';

    /**
     * @var bool
     */
    private $debug;

    /**
     * RequestHandler constructor.
     *
     * @param int $timeout
     * @param string|null $key
     * @param string|null $secret
     * @param string|null $token
     * @param int $version
     */
    public function __construct(
        int $timeout,
        string $key = null,
        string $secret = null,
        string $token = null,
        int $version = 1
    ) {

        if ($timeout < 1) {
            throw new \InvalidArgumentException('$timeout must be positive int ');
        }

        $this->client = new Client();

        $this->timeout = $timeout;

        $this->key = $key;
        $this->secret = $secret;
        $this->token = $token;
        $this->version = $version;
    }

    /**
     * @return int
     */
    public function getVersion(): int
    {
        return $this->version;
    }

    /**
     * @param int $version
     *
     * @return RequestHandler
     */
    public function setVersion(int $version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * @param string $path
     * @param array $parameters
     *
     * @return array
     */
    public function perform(string $path, array $parameters = []): array
    {
        $options = [
            'connect_timeout' => $this->getTimeout(),
            'read_timeout' => $this->getTimeout(),
            'timeout' => $this->getTimeout(),
        ];

        if ($this->hasDebug()) {
            $options['debug'] = $this->getDebug();
        }

        $parameters['api_key'] = $this->key;
        $parameters['api_secret'] = $this->secret;
        $parameters['data_type'] = $this->dataType;

        return $this->handleRequest(
            $this->method,
            sprintf('%s/%s', $this->getRequestUrl(), $path),
            $options,
            $parameters
        );
    }

    /**
     * @return int
     */
    public function getTimeout(): int
    {
        return $this->timeout;
    }

    /**
     * @param int $timeout
     *
     * @return RequestHandlerConfigurable|RequestHandler
     */
    public function setTimeout(int $timeout): RequestHandlerConfigurable
    {
        $this->timeout = $timeout;

        return $this;
    }

    /**
     * @return bool
     */
    public function getDebug(): bool
    {
        return $this->debug;
    }

    /**
     * @return bool
     */
    public function hasDebug(): bool
    {
        return !empty($this->debug);
    }

    /**
     * @param bool $debug
     *
     * @return RequestHandlerConfigurable|RequestHandler
     */
    public function setDebug(bool $debug): RequestHandlerConfigurable
    {
        $this->debug = $debug;

        return $this;
    }

    /**
     * @param string $method
     * @param string $uri
     * @param array $options
     * @param array $parameters
     *
     * @return array
     * @throws ZoomApiCallFailedException
     * @throws ZoomApiCallTimedOutException
     * @throws ZoomApiErrorException
     */
    private function handleRequest(string $method, string $uri = '', array $options = [], array $parameters = []): array
    {
        // Are we going a GET or a POST?
        if (!empty($parameters)) {
            if ('GET' === $method) {
                $options['query'] = $parameters;
            } elseif ('POST' === $method) {
                $options['form_params'] = (object)$parameters;
            } else {
                throw new \InvalidArgumentException('$method must be GET or POST');
            }
        }

        try {
            $response = $this->client->request($method, $uri, $options);
            $response = json_decode($response->getBody(), true);

            if (!empty($response['error'])) {
                throw new ZoomApiErrorException($response['error']['message'], $response['error']['code']);
            }

            return $response;
        } catch (ClientException $exception) {
            $message = $exception->getMessage();
            throw new ZoomApiCallFailedException($message, $exception->getCode(), $exception);
        } catch (RequestException $exception) {
            $message = $exception->getMessage();
            throw new ZoomApiCallTimedOutException($message, $exception->getCode(), $exception);
        }
    }

    /**
     * @return string
     */
    public function getRequestUrl()
    {
        return sprintf("%s/%s", $this->url, sprintf($this->versionUri, $this->version));
    }
}
