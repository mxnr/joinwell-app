<?php

namespace ZoomBundle\Service;

use ZoomBundle\Contract\PacketManager;

/**
 * Class Zoom
 * @package ZoomBundle\Service
 */
class Zoom
{
    private $repository;

    /**
     * Zoom constructor.
     *
     * @param PacketManager $repository
     */
    public function __construct(PacketManager $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return PacketManager
     */
    public function getManager(): PacketManager
    {
        return $this->repository;
    }
}
