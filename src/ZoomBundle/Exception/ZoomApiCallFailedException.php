<?php

namespace ZoomBundle\Exception;

/**
 * Class ZoomApiCallFailedException
 * @package ZoomBundle\Exception
 */
class ZoomApiCallFailedException extends \Exception
{

}
