<?php

namespace ZoomBundle\Exception;

/**
 * Class ZoomApiCallTimedOutException
 * @package ZoomBundle\Exception
 */
class ZoomApiCallTimedOutException extends \Exception
{

}
