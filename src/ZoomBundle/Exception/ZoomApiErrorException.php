<?php

namespace ZoomBundle\Exception;

/**
 * Class ZoomApiErrorException
 * @package ZoomBundle\Exception
 */
class ZoomApiErrorException extends \Exception
{

}
