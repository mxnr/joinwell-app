<?php

namespace ZoomBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration
 * @package ZoomBundle\DependencyInjection
 */
class Configuration implements ConfigurationInterface
{
    /**
     * @return TreeBuilder
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('zoom');

        $rootNode
            ->children()
            ->scalarNode('key')->defaultValue('')->end()
            ->scalarNode('secret')->defaultValue('')->end()
            ->scalarNode('token')->defaultValue('')->end()
            ->integerNode('version')->defaultValue(1)->end()
            ->integerNode('timeout')->defaultValue(30)->end();

        return $treeBuilder;
    }
}
