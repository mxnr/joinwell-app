<?php

namespace ZoomBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Reference;
use ZoomBundle\Manager\ApiPacketManager;
use ZoomBundle\Request\RequestHandler;
use ZoomBundle\Service\Zoom;


/**
 * Class ZoomBundleExtension
 * @package ZoomBundle\DependencyInjection
 */
class ZoomExtension extends Extension
{
    /**
     * @param array $configs
     * @param ContainerBuilder $container
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $processor = new Processor();
        $configuration = new Configuration();

        //reading config
        $config = $processor->processConfiguration($configuration, $configs);


        //preparing bundle service params
        $container->setParameter('api.key', $config['key']);
        $container->setParameter('api.secret', $config['secret']);
        $container->setParameter('api.token', $config['token']);
        $container->setParameter('api.version', $config['version']);
        $container->setParameter('timeout', $config['timeout']);


        //configuring request handler
        $container
            ->register('zoom.request_handler', RequestHandler::class)
            ->addArgument('%timeout%')
            ->addArgument('%api.key%')
            ->addArgument('%api.secret%')
            ->addArgument('%api.token%')
            ->addArgument('%api.version%');

        //configuring repository
        $container
            ->register('zoom.apiPacketManager', ApiPacketManager::class)
            ->addArgument((new Reference('zoom.request_handler')));

        //configuring service providers
        $container
            ->register('zoom.service', Zoom::class)
            ->addArgument((new Reference('zoom.apiPacketManager')));
    }
}
